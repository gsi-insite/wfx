(in-package :wfx)

(defclass widget-class (standard-class)
  ((include-bits
    :initarg :include-bits
    :initform nil
    :accessor include-bits
    :documentation "Stuff that needs to be written to the html header to make a widget work. Like a style block or javascript block. See page-include-bits method.")
   (include-js :initarg :include-js
               :initform nil
               :accessor include-js)
   (defer-include-js :initarg :defer-include-js
                     :initform nil
                     :accessor defer-include-js)
   (include-css :initarg :include-css
                :initform nil
                :accessor include-css))
  (:documentation "Meta class for widgets."))

(defclass widget-theme ()
  ((direct-widget-map :initarg :direct-widget-map
                      :initform (make-hash-table :test #'eq)
                      :accessor direct-widget-map)
   (widget-map :initarg :widget-map
               :initform (make-hash-table :test #'eq)
               :accessor widget-map)
   (db :initarg :db
       :initform nil
       :accessor db)
   (report-classes :initarg :report-classes
                   :initform nil
                   :accessor report-classes)))

(defvar *current-theme* nil)

(defun register-widget (theme base-widget-class-name widget-class-name)
  (let ((widget-class (find-class widget-class-name)))
    (setf (gethash base-widget-class-name (widget-map theme))
          (setf (gethash base-widget-class-name (direct-widget-map theme))
                widget-class))
    (setf (gethash widget-class-name (widget-map theme))
          widget-class)))

(defmethod update-dependent (base-class theme &rest args)
  (declare (ignore args))
  (let ((class (gethash (class-name base-class) (widget-map theme))))
    (when class
      (ensure-class-using-class class (class-name class)
                                :metaclass (class-of class)
                                :name (class-name class)
                                :direct-superclasses (cook-inheritance base-class theme)
                                :include-js nil
                                :defer-include-js nil
                                :include-css nil
                                :include-bits nil
                                :allow-other-keys t))))

(defun cook-inheritance (class theme)
  (unless (class-finalized-p class)
    (finalize-inheritance class))
  (let ((direct-map (direct-widget-map theme)))
    (cons class
          (loop for superclass in (class-direct-superclasses class)
                unless (class-finalized-p superclass)
                do (finalize-inheritance superclass)
                nconc
                (loop for class in (class-precedence-list superclass)
                      when (gethash (class-name class) direct-map)
                      collect it)))))

(defun make-theme-class (base-class theme)
  (let* ((class (find-class base-class))
         (name (gensym (format nil "~a-~a" (class-name (class-of theme)) (class-name class)))))
    (add-dependent class theme)
    (ensure-class name
                  :name name
                  :metaclass (class-of class)
                  :direct-superclasses (cook-inheritance class theme))))

(defun widget-implementation-class (base-class)
  (let ((theme *current-theme*))
    (if theme
        (or (gethash base-class (widget-map theme))
            (setf (gethash base-class (widget-map theme))
                  (make-theme-class base-class theme)))
        (find-class base-class))))

(defmethod validate-superclass ((class widget-class)
                                (superclass standard-class))
  t)

(defmethod validate-superclass ((superclass standard-class)
                                (class widget-class))
  t)

(defun all-widget-superclasses (class)
  (labels ((all-superclasses (class)
             (cons class
                   (loop for super in (class-direct-superclasses class)
			 when (typep super 'widget-class)
			 append (all-superclasses super)))))
    (all-superclasses class)))

(defun propogate-include-bits (class)
  (let ((superclasses (all-widget-superclasses class)))
    (setf (include-bits class)
          (alexandria:mappend #'include-bits superclasses))
    (setf (include-js class)
          (alexandria:mappend #'include-js superclasses))
    (setf (defer-include-js class)
          (alexandria:mappend #'defer-include-js superclasses))
    (setf (include-css class)
          (alexandria:mappend #'include-css superclasses))))

(defmethod initialize-instance :after ((class widget-class) &key)
  (propogate-include-bits class))

(defmethod reinitialize-instance :after ((class widget-class) &key)
  (propogate-include-bits class))

;;; Prefix slots with % so that the subclasses don't get mixed up when using
;;; the same slot names.
(defclass widget ()
  ((%name :initarg :name
          :initform nil
          :accessor name)
   (%group-index :initarg :group-index
                 :initform nil
                 :accessor group-index)
   (%data :initarg :data
          :initform nil
          :accessor data)
   (css-class :initarg :css-class
              :initform nil
              :accessor css-class)))

(defmethod print-object ((object widget) stream)
  (if (name object)
      (print-unreadable-object (object stream :type t :identity t)
        (princ (name object) stream))
      (call-next-method)))

(defgeneric render (widget &key &allow-other-keys)
  (:documentation "Renders a widget"))

(defmethod render :before ((widget widget) &key)
  (pushnew widget (dom)))

(defun render-to-string (widget &rest args)
  (with-html-string
    (apply #'render widget args)))

(defun cache (&key script-name)
  (request-page-cache *current-page*))

(defun dom ()
  (request-page-dom *current-page*))

(defun (setf dom) (value)
  (setf (request-page-dom *current-page*) value))

(defun widget-class (class-or-symbol)
  (let ((class-name (etypecase class-or-symbol
                      (symbol class-or-symbol)
                      (class (class-name class-or-symbol)))))
    (widget-implementation-class class-name)))

(defun make-widget (widget-class &rest args
                    &key name group-index &allow-other-keys)
  "This function instanciates a widget or returns the widget from the dom if it already exists. Each request uri has its own hashtable with widgets. The hashtable represents a simple dom. The dom is automatically updated before a request is passed to a hunchentoot handler."
  (let* ((name (or name
                   (string-downcase (if (classp widget-class)
                                        (class-name widget-class)
                                        widget-class))))
         (class (widget-class widget-class))
         (cache (cache))
         (instance (gethash name cache)))
    (cond ((not instance)
           (setf instance (apply #'make-instance class
                                 :name name args))
           (if group-index
               (setf (gethash group-index
                              (setf (gethash name cache)
                                    (make-hash-table :test 'equal)))
                     instance)
               (setf (gethash name cache) instance)))
          (group-index
           (unless (setf instance
                         (gethash group-index instance))
             (setf instance (apply #'make-instance widget-class args)
                   (gethash group-index instance) instance))))
    (if (and instance
	     (not (eq (class-of instance) class)))
	(restart-case
	    (error "Widget \"~a\" of class ~a already exist. Requested ~a class."
		   (name instance)
		   (class-name (class-of instance))
		   (class-name class))
	  (replace-widget ()
	    :report "Replace old widget"
	    (setf (gethash name cache) nil)
	    (apply #'make-instance widget-class args)))
	instance)))

(defgeneric action-handler (widget)
  (:documentation "This method is called after the dom has been updated from parameters.")
  (:method ((widget widget))))

(defgeneric synq-dom (widget)
  (:documentation "Updates widget slots values. Slots that have names that match parameter names are updated with the parameter values."))

(defgeneric synq-data (widget))

(defmethod synq-data ((widget widget))
  (let ((parameters (append (get-parameters *request*)
                            (post-parameters *request*))))
    (if (listp (data widget))
        (dolist (object (data widget))
          (when (typep object 'standard-object)
            (loop for (key . value) in parameters
               for slot = (find-slot key object)
               when slot
               do (update-slot object slot value))))
        (when (typep (data widget) 'standard-object)
          (loop for (key . value) in parameters
                for slot = (find-slot key (data widget))
                when slot
                do (update-slot (data widget) slot value))))))

(defmethod synq-dom ((widget widget))
  (let ((parameters (append (get-parameters *request*)
                            (post-parameters *request*))))
    (when (name widget)
      (loop for (key . value) in parameters
            for slot = (un-widgy-name widget key)
            when slot
            do (update-slot widget slot value)))))

(defun map-dom (function)
  (map nil function
       (dom)))

(defun handle-messages ()
  (let* ((widget (get-widget (post-parameter "message-dest")))
         (type (post-parameter "message-type"))
         (action (and type 
                      (find-symbol (string-upcase type) :keyword))))
    (when (and widget action)
      (handle-action widget action))))

(defun map-dom-events ()
  (map-dom #'action-handler)
  (handle-messages))

(defun js-inclusion-string (path)
  (format nil "<script type='text/javascript' src='~a'></script>" path))

(defun js-defer-inclusion-string (path)
  (format nil "<script defer type='text/javascript' src='~a'></script>" path))

(defun css-inclusion-string (path)
  (format nil "<link  rel='stylesheet' href='~a' />" path)) ;; type='text/css'

(defun print-included (accessor &key key)
  (let ((printed (make-hash-table :test #'equal)))
    (map-dom
     (lambda (value)
       (when (subtypep (class-of (class-of value)) 'widget-class)
         (loop for css in (funcall accessor (class-of value))
               unless (gethash css printed)
               do
               (princ (if key
                          (funcall key css)
                          css))
               (setf (gethash css printed) t)))))))

(defun page-include-css ()
  (print-included #'include-css :key #'css-inclusion-string))

(defun page-include-js ()
  (print-included #'include-js :key #'js-inclusion-string))

(defun page-defer-include-js ()
  (print-included #'defer-include-js :key #'js-defer-inclusion-string))

(defun widget-include-bits (widget-class-instance)
  "Returns the include statements for the widget's include files."
  (when widget-class-instance
    (map nil #'princ (include-bits widget-class-instance))))

(defun page-include-bits ()
  "Takes stuff that needs to be written to the html that is independant of the rendering of the actual widget. The method page-include-bits goes through all the widgets for a page on in the dom and adds any include-bits found and this method needs to be explicitly called in html page."
  (print-included #'include-bits))

(defun get-widget (name &key group-index)
  (let* ((cache (cache))
         (instance (gethash name cache)))
    (if (and group-index name)
        (gethash group-index instance)
        instance)))

(defun set-widget (instance &key group-index)
  "Adds a widget to the dom."
  (let ((cache (cache))
        (name (name instance)))
    (if group-index
        (setf (gethash name cache)
              (make-hash-table :test 'equal)
              (gethash group-index (gethash name cache))
              instance)
        (setf (gethash name cache) instance))
    instance))

(defgeneric theme-initargs (widget theme))
(defmethod theme-initargs ((widget t) (theme t))
  nil)

(defmethod initialize-instance :around ((widget widget) &rest initargs)
  (apply #'call-next-method widget (append (theme-initargs widget *current-theme*) initargs)))
