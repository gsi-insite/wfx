(in-package :common-lisp-user)

(defpackage :wfx
  (:use :c2cl :hunchentoot :cl-who :dx-utils
        :wfx-script)
  (:nicknames :dx-base-site)
  (:export

   ;;utils
   #:*indent-code*
   #:with-debugging
   #:with-html
   #:with-html-string
   #:with-parameters
   #:sub-name
   #:widgy-name
   #:un-widgy-name
   #:synq-edit-data

   ;;events
   #:window-events
   #:on-after-print
   #:on-before-print
   #:on-before-unload
   #:on-error
   #:on-has-change
   #:on-load
   #:on-message
   #:on-offline
   #:on-online
   #:on-page-hide
   #:on-page-show
   #:on-pop-state
   #:on-redo
   #:on-resize
   #:on-storage
   #:on-undo
   #:on-unload
   
   #:form-events
   #:on-blur
   #:on-change
   #:on-context-menu
   #:on-focus
   #:on-form-change
   #:on-form-input
   #:on-input
   #:on-valid
   #:on-select
   #:on-submit

   #:keyboard-events
   #:on-key-down
   #:on-key-press
   #:on-key-up

   #:mouse-events
   #:on-click
   #:on-dbl-click
   #:on-drag
   #:on-drag-end
   #:on-drag-enter
   #:on-drag-leave
   #:on-drag-over
   #:on-drag-start
   #:on-drop
   #:on-mouse-down
   #:on-mouse-move
   #:on-mouse-out
   #:on-mouse-over
   #:on-mouse-up
   #:on-mouse-wheel
   #:on-scroll

   #:media-events
   #:on-abort

   ;;html-element
   #:html-element
   #:access-key
   #:css-class
   #:content-editable
   #:context-menu
   #:dir
   #:draggable
   #:dropped-zone
   #:hidden
   #:id
   #:lang
   #:spell-check
   #:style
   #:tab-index
   #:title
   #:translate

   ;;widget
   #:widget-class
   #:include-bits
   #:include-js
   #:include-css

   #:widget
   #:name
   #:group-index
   #:data

   #:render
   #:render-to-string
   #:clear-cache
   #:make-widget
   #:synq-dom
   #:synq-data
   #:map-dom
   #:page-include-css
   #:page-include-js
   #:page-include-bits
   #:get-widget

   ;;ajax
   #:ajax-widget

   #:js-render
   #:js-render-form-values

   ;;js
   #:defer-js
   #:defer-js-function
   #:deferred-js
   #:js-link
   #:js-pair
   #:js-value

   ;;framework-widgets
   #:container
   #:content

   #:div

   #:form
   #:accept-charset
   #:action
   #:auto-complete
   #:form-enc-type
   #:form-method
   #:form-no-validate
   #:target

   #:input
   #:accept
   #:alt
   #:auto-focus
   #:checked
   #:disabled
   #:form-action
   #:form-target
   #:height
   #:data-list
   #:element-max
   #:max-length
   #:element-min
   #:multiple
   #:element-name
   #:pattern
   #:place-holder
   #:read-only
   #:required
   #:size
   #:src
   #:element-step
   #:element-type
   #:value
   #:width
   #:submit-on-change

   #:get-attributes
   #:html-from-list

   #:text-area
   #:cols
   #:rows
   #:wrap

   #:checkbox
   #:description

   #:checkbox-list
   #:items
   #:checkboxes
   #:orientation
   #:check-all

   #:select
   #:first-item
   #:blank-allowed
   #:allow-deselect

   #:with-box
   
   #:layout-column

   #:layout-row
   #:layout-row-class
   #:columns

   
   #:box-container
   #:box-content
   #:box-footer
   
   #:box
   #:header
   #:footer

   #:tab-box-header
   #:tabs
   #:js-id

   #:tab-box-content

   #:tab-box

   #:simple-form
   #:action-title
   #:form-id
   #:ajax-render-widget
   #:parent-widget

   #:form-section
   #:section-size

   ;;grid
   #:grid
   #:table-class
   #:site-url
   #:row-object-class
   #:total-row-count
   #:rows-per-page
   #:editing-row
   #:error-message
   #:search-term
   #:sort-direction
   #:sort-column
   #:sort-key-function
   #:not-sorting-columns
   #:initial-sort-column
   #:grid-filter
   #:state
   #:parent-grid
   #:toolbar-widget
   #:buttons
   #:additional-buttons
   #:grid-error
   #:grid-filtered-rows
   #:selected-row
   #:update-table
   #:grid-column
   #:printer
   #:special-printer

   #:scroll-to
   #:process-data-table

   #:button
   #:event
   #:url
   #:text
   #:icon
   #:hint
   #:confirm
   #:permission
   #:permission-check

   #:meta-tag
   #:meta-type
   #:meta-name
   #:link
   #:link-type
   #:rel
   #:href
   #:href-lang
   #:media
   #:link-condition


   #:style-type
   #:scope

   #:script
   #:script-type
   #:async
   #:charset
   #:defer
   #:page-bottom-p
   #:script-condition

   #:page
   #:doc-type
   #:manifest
   #:base
   #:meta-tags
   #:links
   #:styles
   #:scripts
   #:no-script
   #:body-class

   #:labeled-input
   #:label
   #:register-widget
   #:*current-theme*
   #:widget-theme
   #:widget-map
   #:author
   #:classification
   #:key-words
  
   #:make-dx-site
   #:site-acceptor
   #:debug-errors-p
   #:logging-p
   #:started
   #:site-name
   #:site-url
   #:login-url
   #:data-path
   #:permissions
   #:check-page-permission
   #:check-page-permission-or-error
   #:define-dx-handler
   #:define-dx-ajax
   #:defer-js
   #:defer-js-function
   #:deferred-js
   #:super-user-p
   #:render-error-page
   #:render-permission-denied-page
   #:check-permission
   #:*development-mode*
   #:acceptor*
   #:check-permission-or-error
   #:current-user
   #:redirect-after-login
   #:login-not-required
   #:make-icon
   #:icon-name
   #:editable
   #:get-rows
   #:theme-initargs
   #:icon-size
   #:collapsible
   #:render-row-editor
   #:edit-form
   #:finish-editing
   #:table-type 
   #:delete-message
   #:allowed-actions
   #:editor
   #:css-span
   #:action-widget
   #:js-callback
   #:sortable
   #:searchable
   #:handle-action
   #:grid-edit-form
   #:grid-button
   #:action-handler
   #:js-render-return-false
   #:*new-bottom-button*
   #:*view-button*
   #:grid-bottom-button
   #:test
   #:bottom-buttons
   #:header-content
   #:grid-filter-selector
   #:*in-ajax-request*
   #:list-grid-filters
   #:apply-grid-filter
   #:export-csv
   #:item-value
   #:init-data-table
   #:render-row-actions
   #:modal-edit
   #:grid-editor
   #:get-data-table-rows
   #:*edit-button*
   #:*delete-button*
   #:set-current-row
   #:render-header-buttons
   #:clear-filter-title
   #:body-content
   #:sort-key
   #:grid-redirect
   #:grid-read-only
   #:update-one-table
   #:js-apply
   #:with-html-string-no-indent
   #:with-html-no-indent
   #:defer-include-js
   #:page-defer-include-js
   #:open-dialog
   #:close-dialog
   #:js-checkbox-value
   #:prefix-buttons
   #:*widget-parameters*
   #:db
   #:before-submit
   #:test-grid-action
   #:test-action
   #:item-equal
   #:bad-request
   #:ensure-request-page
   #:find-page
   #:*current-page*
   #:request-page
   #:nil
   #:request-page-p
   #:copy-request-page
   #:request-page-name
   #:request-page-id
   #:request-page-dom
   #:request-page-cache
   #:request-page-permissions
   #:generate-new-page
   #:ajax-processor
   #:theme
   #:find-request-page
   #:*page-id-lock*
   #:request-page-claimed
   #:request-page-new
   #:page-id
   #:parameters
   #:map-dom-events
   #:page-permissions
   #:advanced-search
   #:advanced-search-slot-package
   #:include-hidden
   #:make-permission
   #:permission-p
   #:copy-permission
   #:permission-page
   #:permission-sub-permissions
   #:permission-entities
   #:setup-request-page
   #:*current-permissions*
   #:js-render-disabled-form-values
   #:make-current-permissions
   #:current-permissions
   #:current-permissions-p
   #:copy-current-permissions
   #:current-permissions-time
   #:current-permissions-permissions
   #:render-inline-buttons
   #:render-export-button
   #:request-page-reuse
   #:grid-alert
   #:report-classes
   #:value-select
   #:compile-grid-script
   #:*script-error*
   #:render-bottom-buttons
   #:render-save-buttons
   #:modal
   #:save-button
   #:bare-page
   #:should-escape
   #:authenticate-token
   #:abort-api-request
   #:script-key
   #:filter-first-item))


