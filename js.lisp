(in-package :wfx)

(defvar *widget-parameters*)

(defun defer-js (format-arguments &rest args)
  (when format-arguments
    (push (apply #'format nil format-arguments args)
          (getf *widget-parameters* :javascript-defer))))

(defun defer-js-function (format-arguments &rest args)
  (when (and format-arguments
             (not *in-ajax-request*))
    (push (apply #'format nil format-arguments args)
          (getf *widget-parameters* :javascript-defer-function))))

(defun deferred-js ()
  (format nil "$(document).ready(function(){~{~a;~}});~{~a~}"
          (getf *widget-parameters* :javascript-defer)
          (getf *widget-parameters* :javascript-defer-function )))

(defun js-link (&rest code)
  (frmt "javascript:{~{~a~^,~}}" code))

(defun js-pair (key value)
  (frmt "[~s, ~s]" key value))

(defun js-value (widget &optional id)
  (let ((name (if (typep widget 'widget)
                  (name widget)
                  widget)))
    (format nil "[~s, document.getElementById(~s).value]"
            name (or id name))))

(defun js-checkbox-value (widget)
  (let ((name (if (typep widget 'widget)
                  (name widget)
                  widget)))
    (format nil "[~s, $(\"#~a\").prop(\"checked\")]"
            name name)))
