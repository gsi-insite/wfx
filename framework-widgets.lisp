(in-package :wfx)

;;TODO: Does this work right?
(defmacro with-box ((&rest args) &body body)
  `(render (make-instance 'box ,@args
                          :content (with-html-string
                                     ,@body))))


(defclass layout-column (container)
  ((class :initarg :class
          :accessor layout-row-class)
   (width :initarg :width
           :accessor :width)))

(defgeneric width-to-class (column &key))

(defmethod width-to-class ((column layout-column) &key)
  (format nil "~A ~A" (width column) (layout-row-class column)))

(defmethod render ((column layout-column) &key width-to-class-p body)
  (with-html
    (:div :class (if width-to-class-p
                     (width-to-class column)
                     (layout-row-class column))
          (render column :body body))))

(defclass layout-row (container)
  ((class :initarg :class
          :accessor layout-row-class)
   (columns :initarg :columns
              :accessor columns)))

(defmethod render ((row layout-row) &key body)
  (with-html
    (:div :class (layout-row-class row)
          (cond ((columns row)
                 (dolist (column (columns row))
                    (render column)))
                (t
                  (render (content row) :body body))))))
;;;

(defclass box (container)
  ((header :initarg :header
           :initform nil
           :accessor header)
   (header-content :initarg :header-content
                   :initform nil
                   :accessor header-content)
   (icon :initarg :icon
         :initform nil
         :accessor icon)
   (icon-size :initarg :icon-size
              :initform 16
              :accessor icon-size)
   (collapsible :initarg :collapsible
                :initform t
                :accessor collapsible)))

(defmethod render ((box box) &key)
  (render (content box)))

;;;

(defclass tab-box-header (box-container)
  ((tabs :initarg :tabs
         :initform nil
         :accessor tabs)
   (title :initarg :title
          :initform nil
          :accessor title)
   (js-id :initarg :js-id
          :initarg nil
          :accessor js-id)))

(defmethod render ((widget tab-box-header) &key tabs)
  (with-html
    (:ul :class (css-class widget)   
         (loop for (title) in (or (tabs widget) tabs)
            for i from 0
            do
              (htm
               (:li (:a 
                     :href (format nil "#~a-tab-~a" (js-id widget) i)
                     (esc title))))))
    (when (title widget)
      (htm
       (esc (title widget))))))

(defclass tab-box-content (box-content)
  ((tabs :initarg :tabs
         :initform nil
         :accessor tabs)
   (js-id :initarg :js-id
          :initarg nil
          :accessor js-id)))

(defmethod render ((widget tab-box-content) &key tabs)
  (with-html
    (loop for (nil content) in (or (tabs widget) tabs)
                            for i from 0
                            do
                            (htm
                             (:div :id (format nil "~a-tab-~a" (js-id widget) i)
                                   (str content))))))

(defclass tab-box (box)
  ((tabs :initarg :tabs
         :initform nil
         :accessor tabs)
   (title :initarg :title
          :initform nil
          :accessor title)
   (body-content :initarg :body-content
                 :initform nil
                 :accessor body-content)))

(defmethod event-handler ((box tab-box) (event (eql :init-box-container))
                               &key content)
  (make-widget 'box-container 
               :name (sub-name box "tab-box-container")
               :content content))

(defmethod event-handler ((box tab-box) (event (eql :init-box-header))
                            &key content)
  (make-widget 'tab-box-header :name 
               (sub-name box "tab-box-header") 
               :content content))

(defmethod event-handler ((box tab-box) (event (eql :init-box-content))
                             &key content)
  (make-widget 'tab-box-content :name 
               (sub-name box "tab-box-content")
               :content content))

(defmethod event-handler ((box tab-box) (event (eql :init-box-footer))
                            &key content)
  (make-widget 'box-footer :name 
               (sub-name box "tab-box-footer") 
               :content content))


(defmethod render ((widget tab-box) &key)
   (let ((header (or (header widget)
                    (event-handler widget :init-box-header)))
        (content (or (content widget)
                     (event-handler widget :init-box-content))))
 
     (setf (js-id header) (sub-name widget "tabid"))
     (setf (tabs header) (tabs widget))
     (setf (title header) (title widget))

     (setf (js-id content) (sub-name widget "tabid"))
     (setf (tabs content) (tabs widget))

     (setf (header widget) header)
     (setf (content widget) content))
  
  (call-next-method))

(defclass button (widget)
  ((event :initarg :event
          :initform nil
          :accessor event)
   (url :initarg :url
        :initform nil
        :accessor url)
   (text :initarg :text
         :initform nil
         :accessor text)
   (icon :initarg :icon
         :initform nil
         :accessor icon) 
   (hint :initarg :hint
         :initform nil
         :accessor hint)
   (confirm :initarg :confirm
            :initform nil
            :accessor confirm)
   (permission :initarg :permission
               :initform t
               :accessor permission)
   (permission-check
    :initarg :permission-check
    :initform nil
    :accessor permission-check)))

(defmethod render ((button button) &key parent-widget form-id)
  (with-slots (hint icon text event url confirm permission permission-check) button
    (when (or (null permission) 
              (and permission 
                   (if permission-check
                       (funcall permission-check permission)
                       t)))
      (with-html
        (:button :class (css-class button)
             :onclick
             (format nil "event.preventDefault(); ~a"
                     (if form-id
                         (js-render-form-values parent-widget
                            form-id                            
                            (js-pair "event" (event button)))
                         (js-render parent-widget 
                                    (js-pair "event" (event button)))))
             (str (text button)))))))

(defclass simple-form (container)
  ((width :initarg :width
              :initform nil
              :accessor width)
   (parent-widget :initarg :parent-widget
                  :initform nil
                  :accessor parent-widget)
   (form-id :initarg :form-id
            :initform nil
            :accessor form-id)
   (ajax-render-widget :initarg :ajax-render-widget
                       :initform nil
                       :accessor ajax-render-widget)))

(defmethod event-handler ((form simple-form) (event (eql :init-simple-form-box))
                          &key)
  (make-widget 'box :name 
               (sub-name form "box")))

(defmethod event-handler ((form simple-form) (event (eql :init-simple-form-buttons))
                          &key)
  (list
    (make-widget 'wfx:button
                :name "close-button"
                :event "close" 
                :text "Close"
                ;;:css-class "button grey fl-space"
                )))

(defmethod render ((widget simple-form) &key body)
  (let* ((name (sub-name widget "form"))
         (box (event-handler widget :init-simple-form-box))
         (form (make-widget 'form 
                            :name name
                            :id (or (form-id widget)
                                    name)
                            :method "post"
                            :on-submit "return false;")))
    ;(setf (container box) (event-handler box :init-box-container))
    (setf (content box) form)

    (setf (content form) 
            (with-html-string
                     (if body
                         (render body)
                         (render (content widget)))
                     (let ((buttons (event-handler widget :init-simple-form-buttons)))
                       (dolist (button buttons)
                         (htm
                          (render button (parent-widget widget)
                                         :form-id (form-id widget)))))))

    (render box)))


(defclass form-section (widget)
  ((section-size :initarg :section-size
                 :initform 100
                 :accessor section-size)))

(defmethod render ((widget form-section) &key label input label-for)
  (with-html
    (:div :class "control-group"
          (:label :id (strip-name label) :class "control-label docs" :for label-for
                  (str label))
          (:div :class "controls"
                (str input)))))

(defclass modal (widget)
  ((header :initarg :header
           :initform nil
           :accessor header)
   (content :initarg :content
            :initform nil
            :accessor content)
   (save-button :initarg :save-button
                :initform t
                :accessor save-button)
   (buttons :initarg :buttons
            :initform nil
            :accessor buttons)
   (grid :initarg :grid
         :initform nil
         :accessor grid)
   (size :initarg :size
         :initform :large
         :accessor size)
   (form-id :initarg :form-id
            :initform nil
            :accessor form-id))
  (:metaclass widget-class))
