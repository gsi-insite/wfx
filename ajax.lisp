(in-package :wfx)

(defclass ajax-widget (widget)
  ()
  (:metaclass widget-class))

(defvar *in-ajax-request* nil)

(defmethod render :around ((widget ajax-widget) &key from-ajax)
  (if from-ajax
      (let ((*in-ajax-request* widget))
        (call-next-method))
      (with-html
        (:div :id (name widget)
              ;;:class (css-class widget)
              (call-next-method)))))

(defun js-render (widget &rest args-scripts)
  (format nil "ajax_render(~s, ~s~@[, [~{~a~^,~}]~])"
          (script-name*)
          (if (typep widget 'widget)
              (name widget)
              widget)
          args-scripts))

(defun js-render-form-values (widget form-name
                              &rest args-scripts)
  (format nil "ajax_render(~s, ~s, get_form_values(~s)~@[.concat([~{~a~^,~}])~])"
          (script-name*)
          (if (typep widget 'widget)
              (name widget)
              widget)
          form-name
          args-scripts))

(defun js-render-disabled-form-values (widget form-name
                                       &rest args-scripts)
  (format nil "ajax_render(~s, ~s, get_form_values(~s,true)~@[.concat([~{~a~^,~}])~])"
          (script-name*)
          (if (typep widget 'widget)
              (name widget)
              widget)
          form-name
          args-scripts))

(defun js-render-return-false (widget &rest args-scripts)
  (format nil "~a; return false;"
          (apply #'js-render widget args-scripts)))

(defun js-apply (widget &rest args)
  (format nil "ajax_render(~s, ~s, [~{~a~^,~}].concat(~a))"
          (script-name*)
          (if (typep widget 'widget)
              (name widget)
              widget)
          (butlast args)
          (car (last args))))
