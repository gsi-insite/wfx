var afterLoad = [];

function arrayPosition(array, string) {
    for (var i = 0; i < array.length; ++i) {
        if (array[i] === string)
            return i;
    }
    return -1;
}

window.onload = function ()
{
    for (var i = 0; i < afterLoad.length; i++)
        afterLoad[i]();
}

function updateTable(table) {
    jQuery('#' + table).dataTable().fnDraw();
}

function scroll_to(id) {
    $('html, body').animate({scrollTop: $('#' + id).offset().top - 5});
}
