(in-package :wfx)

(defmacro with-debugging (&body body)
  ;; Using this as debugging tool because hunchentoot
  ;; swallows all errors here if set to swallow errors.
  `(handler-bind ((error #'invoke-debugger))
     ,@body))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *indent-code* t))

;;(defmacro with-html-to-string ((&key prologue (indent '*indent-code*)) &body body)
;;  `(with-html-output-to-string (*standard-output* nil :indent ,indent
;;                                                  :prologue ,prologue)
;;     ,@body))

(defmacro with-html (&body body)
  `(with-html-output (*standard-output* nil :indent *indent-code*)
     ,@body))

(defmacro with-html-no-indent (&body body)
  `(with-html-output (*standard-output* nil :indent nil)
     ,@body))

(defmacro with-html-string (&body body)
  `(with-html-output-to-string (*standard-output* nil :indent *indent-code*)
     ,@body))

(defmacro with-html-string-no-indent (&body body)
  `(with-html-output-to-string (*standard-output* nil :indent nil)
     ,@body))

(defmacro with-parameters (parameters &body body)
  "with-parameters ((variable-name \"parameter-name\") | variable-name) body"
  `(let ,(loop for parameter in parameters
               for (variable parameter-name) =
               (if (consp parameter)
                   parameter
                   (list parameter
                         (string-downcase parameter)))
               collect `(,variable (parameter ,parameter-name)))
     ,@body))

(defun to-html (string)
  (declare ((simple-array character (*)) string))
  (substitute #\_ #\- string))

(defun nto-html (string)
  (declare ((simple-array character (*)) string))
  (loop for i below (length string)
        for char = (char string i)
        when (char= char #\-)
        do (setf (char string i) #\_))
  string)

(defun from-html (string)
  (declare ((simple-array character (*)) string))
  (substitute #\- #\_ string))

(defun nfrom-html (string)
  (declare ((simple-array character (*)) string))
  (loop for i below (length string)
        for char = (char string i)
        when (char= char #\_)
        do (setf (char string i) #\-))
  string)

(defun sub-name (widget name)
  (concatenate 'string (name widget) "-" name))

(defun widgy-name (instance slot-name)
  (nto-html
   (concatenate 'string
                (name instance)
                "."
                slot-name)))

(defun find-slot-definition (slot-name object)
  (find slot-name (class-slots (class-of object))
        :key #'slot-definition-name
        :test #'string-equal))

(defun find-direct-slot-definition (class slot-name)
  (labels ((find-slot (class)
             (or (find slot-name (class-direct-slots class)
                       :key #'slot-definition-name)
                 (some #'find-slot (class-direct-superclasses class)))))
    (find-slot class)))

(defun find-slot (slot-name object)
  (let ((slot (find-slot-definition slot-name object)))
    (when slot
      (slot-definition-name slot))))

(defun find-slot-definition* (slot-name object start)
  (declare ((simple-array character (*)) slot-name)
           (fixnum start)
           (optimize speed))
  (let ((length1 (- (length slot-name) start)))
   (loop for slot in (class-slots (class-of object))
         for name = (the (or
                          (simple-array character (*))
                          (simple-array base-char (*)))
                         (string (the symbol (slot-definition-name slot))))
         for length2 = (length name)
         when (and (= length1 length2)
                   (loop for i below length2
                         for j from start
                         for char1 of-type base-char = (char slot-name j)
                         for char2 of-type base-char = (char name i)
                         always (or (char-equal char1 char2)
                                    (and (char= char1 #\_)
                                         (char= char2 #\-)))))
         return slot)))

(defun un-widgy-name (instance name)
  (declare (string name)
           (optimize speed))
  (let* ((instance-name (name instance))
         (length1 (length name))
         (length2 (length instance-name)))
    (declare (string instance-name))
    (when (and (> length1 length2)
               (char= (char name length2) #\.))
      (let ((dot
              (loop for i below length2
                    for char1 = (char name i)
                    for char2 = (char instance-name i)
                    always (or (char= char1 char2)
                               (and (char= char1 #\_)
                                    (char= char2 #\-)))
                    finally (return i))))
        (when dot
          (find-slot-definition* name instance (1+ dot)))))))

(defun get-slot (instance slot-name)
  (if (slot-boundp instance slot-name)
      (slot-value instance slot-name)))

(defun update-slot (instance slot value)
  (let* ((slot (if (typep slot 'slot-definition)
                   slot
                   (find-slot-definition slot instance)))
         (parser (and (typep slot 'xdb2:storable-slot)
                      (xdb2:slot-parser slot)))
         (value (if parser
                    (funcall parser value)
                    value)))
    (cond ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :list))
           (push value (slot-value-using-class (class-of instance) instance slot)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :number))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-number value)
                     value)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :integer))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-integer value)
                     value)))
          (t
           (setf (slot-value-using-class (class-of instance) instance slot)
                 value)))))

(defun prepare-slots (object)
  (when (typep object 'xdb2:storable-object)
    (loop for slot in (class-slots (class-of object))
          when (and (typep slot 'xdb2:storable-slot)
                    (eq (xdb2:db-type slot) :list))
          do (setf (slot-value-using-class (class-of object) object slot) nil))))

(defun synq-edit-data (object)
  (let ((parameters (append (get-parameters *request*)
                            (post-parameters *request*))))
    (when (typep object 'standard-object)
      (prepare-slots object)
      (loop for (key . value) in parameters
            for slot = (find-slot key object)
            when slot
            do (update-slot object slot (strip-white-space value))))))

(defun build-validation-array (validation-list)
  (when validation-list
    (json:encode-json-to-string
     (loop for (element type required) in validation-list
           collect `((element . ,element)
                     (type . ,type)
                     (required . ,(equalp required "required")))))))
