(defsystem wfx
  :name "wfx"
  :version "0.1"
  :depends-on (:hunchentoot :cl-who :closer-mop :cl-ppcre :alexandria :cl-json 
               :dx-utils :ht-simple-ajax
               :xdb2 wfx-script)
  :serial t
  :components ((:file "packages")
               (:file "utils")
               (:file "html-events")
               (:file "html-elements")
               (:file "widget")
               (:file "ajax")
               (:file "js")
               (:file "permissions")
               (:file "requests")
               (:file "base-widgets")
               (:file "framework-widgets")
               (:file "grid")))
