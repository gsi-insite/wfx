(in-package :wfx-examples)

(defun mapc-directory-tree (fn directory)
  (dolist (entry (cl-fad:list-directory directory))
    (when (cl-fad:directory-pathname-p entry)
      (mapc-directory-tree fn entry))
    (funcall fn entry)))


(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))

(mapc-directory-tree (lambda (x)
		       (when (or (equal (pathname-type x) "js") (equal (pathname-type x) "css") (equal (pathname-type x) "png") (equal (pathname-type x) "jpg")
				 (equal (pathname-type x) "gif") (equal (pathname-type x) "htm") (equal (pathname-type x) "pdf"))

			 (push (create-static-file-dispatcher-and-handler 
				(format nil "/~A" (replace-all (format nil "wfx/examples/~A" (pathname x)) "~/wfx/examples/" "")) 
				(format nil "~A" (pathname x))
				) *dispatch-table*)))
		     "~/wfx/examples/")