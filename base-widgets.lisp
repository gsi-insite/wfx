(in-package :wfx)

(defun get-attributes (widget exclude-list)
  (let ((attributes))
    (dolist (slot (class-slots (class-of widget)))
      (let* ((slot-name (slot-definition-name slot))
            (value (slot-value widget slot-name)))
        (when value
          (unless (find slot-name (append exclude-list '(%name)) :test #'string-equal)
            (setf attributes (append attributes
                                     (list (intern-key (slot-definition-name slot))
                                           (cond ((equal (slot-definition-name slot)
                                                         "disabled")
                                                  (if value
                                                      "disabled"))
                                                 ((equal (slot-definition-name slot)
                                                         "checked")
                                                  (if value
                                                      "checked"))
                                                 ((equal (slot-definition-name slot)
                                                         "readonly")
                                                  (if value
                                                      "readonly"))
                                                 (t
                                                  value)))))))))
    attributes))

(defun html-from-list (list)
  (cl-who::string-list-to-string (cl-who::tree-to-template list)))

(defmethod render ((integer integer) &key)
  (with-html
    (str integer)))

(defmethod render ((float float) &key)
  (with-html
    (str float)))

(defmethod render ((number number) &key)
  (with-html
    (str number)))

(defmethod render ((string string) &key)
  (with-html
    (str string)))

(defclass container (widget)
  ((content :initarg :content
             :initform nil
             :accessor content)))

(defmethod render ((container container) &key)
  (when (content container)
      (render (content container))))

(defmethod render ((container container) &key body)
  (with-html
    (cond (body
           (render body))
          (t
           (render container)))))

(defclass div (container html-element mouse-events form-events)
  ())

(defmethod render ((widget div) &key body)
  (with-html
    (str
     (html-from-list
      `((:div
         :id ,(or (id widget) (name widget))
         ,@(get-attributes widget (list 'id))
         ,(cond (body
                 (render body))
                (t
                 (render (content widget))))))))))

(defclass form (container html-element form-events)
  ((accept-charset :initarg :accept-charset
         :initform nil
         :accessor accept-charset
         :documentation "Specifies the character encodings that are to be used for the form submission - http://www.w3schools.com/tags/att_form_accept_charset.asp")
   (action :initarg :action
         :initform nil
         :accessor action
         :documentation "Specifies where to send the form-data when a form is submitted - http://www.w3schools.com/tags/att_form_action.asp")
   (auto-complete :initarg :auto-complete
         :initform nil
         :accessor auto-complete
         :documentation "Specifies whether a form should have autocomplete on or off - http://www.w3schools.com/tags/att_form_autocomplete.asp")
   (form-enc-type :initarg :form-enc-type
         :initform nil
         :accessor form-enc-type
         :documentation "Specifies how the form-data should be encoded when submitting it to the server (only for method='post') - http://www.w3schools.com/tags/att_form_enctype.asp")
   (method :initarg :method
         :initform nil
         :accessor form-method
         :documentation "Specifies the HTTP method to use when sending form-data - http://www.w3schools.com/tags/att_form_method.asp")
   (form-no-validate :initarg :form-no-validate
         :initform nil
         :accessor form-no-validate
         :documentation "Specifies that the form should not be validated when submitted - (* IE only from v10) - http://www.w3schools.com/tags/att_form_novalidate.asp")
   (target :initarg :target
         :initform nil
         :accessor target
           :documentation "Specifies where to display the response that is received after submitting the form - ")))

(defgeneric render-save-buttons (widget form-id &key &allow-other-keys))

(defmethod render ((widget form) &key body)
  (with-html
    (str
     (html-from-list
      `((:form
         :name ,(name widget)
         :id ,(or (id widget) (name widget))
         :method ,(or (form-method widget) :post)
         :target ,(target widget)
         ,@(get-attributes widget (list 'method 'name 'id 'value 'target 'content))
         (:fieldset
          ;;(:hidden :type "hidden" :name "form-id" :value ,(or (id widget) (name widget)))
          ,(or body (content widget)))))))))

(defclass input (widget html-element mouse-events form-events)
  ((accept :initarg :accept
         :initform nil
         :accessor accept
         :documentation "Specifies the types of files that the server accepts (only for type='file') - http://www.w3schools.com/tags/att_input_accept.asp")
   (alt :initarg :alt
         :initform nil
         :accessor alt
         :documentation "Specifies an alternate text for images (only for type='image') - http://www.w3schools.com/tags/att_input_alt.asp")
   (auto-complete :initarg :auto-complete
         :initform nil
         :accessor auto-complete
         :documentation "Specifies whether an <input> element should have autocomplete enabled - http://www.w3schools.com/tags/att_input_autocomplete.asp")
   (auto-focus :initarg :auto-focus
         :initform nil
         :accessor auto-focus
         :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (checked :initarg :checked
         :initform nil
         :accessor checked
         :documentation "Specifies that an <input> element should be pre-selected when the page loads (for type='checkbox' or type='radio') - http://www.w3schools.com/tags/att_input_checked.asp")
   (disabled :initarg :disabled
         :initform nil
         :accessor disabled
         :documentation "Specifies that an <input> element should be disabled - http://www.w3schools.com/tags/att_input_disabled.asp")
   (form :initarg :form
         :initform nil
         :accessor form
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (form-action :initarg :form-action
         :initform nil
         :accessor form-action
         :documentation "Specifies the URL of the file that will process the input control when the form is submitted (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formaction.asp")
   (form-enc-type :initarg :form-enc-type
         :initform nil
         :accessor form-enc-type
         :documentation "Specifies how the form-data should be encoded when submitting it to the server (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formenctype.asp")
   (form-method :initarg :form-method
         :initform nil
         :accessor form-method
         :documentation "Defines the HTTP method for sending data to the action URL (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formmethod.asp")
   (form-no-validate :initarg :form-no-validate
         :initform nil
         :accessor form-no-validate
         :documentation "Defines that form elements should not be validated when submitted - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formnovalidate.asp")
   (form-target :initarg :form-target
         :initform nil
         :accessor form-target
         :documentation "Specifies where to display the response that is received after submitting the form (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formtarget.asp")
   (height :initarg :height
         :initform nil
         :accessor height
         :documentation "Specifies the height of an <input> element (only for type='image') - http://www.w3schools.com/tags/att_input_height.asp")
   (list :initarg :list
         :initform nil
         :accessor data-list
         :documentation "Refers to a <datalist> element that contains pre-defined options for an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_list.asp")
   (max :initarg :max
         :initform nil
         :accessor element-max
         :documentation "Specifies the maximum value for an <input> element - (* IE only from v10 and not for date/time) - http://www.w3schools.com/tags/att_input_max.asp")
   (max-length :initarg :max-length
         :initform nil
         :accessor max-length
         :documentation "Specifies the maximum number of characters allowed in an <input> element - http://www.w3schools.com/tags/att_input_maxlength.asp")
   (min :initarg :min
         :initform nil
         :accessor element-min
         :documentation "Specifies a minimum value for an <input> element - (* IE only from v10 and not for date/time) - http://www.w3schools.com/tags/att_input_min.asp")
   (multiple :initarg :multiple
         :initform nil
         :accessor multiple
         :documentation "Specifies that a user can enter more than one value in an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_multiple.asp")
   (element-name :initarg :element-name
         :initform nil
         :accessor element-name
         :documentation "Specifies the name of an <input> element - http://www.w3schools.com/tags/att_input_name.asp")
   (pattern :initarg :pattern
         :initform nil
         :accessor pattern
         :documentation "Specifies a regular expression that an <input> element's value is checked against - (* IE only from v10) - http://www.w3schools.com/tags/att_input_pattern.asp")
   (place-holder :initarg :place-holder
         :initform nil
         :accessor place-holder
         :documentation "Specifies a short hint that describes the expected value of an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_placeholder.asp")
   (read-only :initarg :read-only
         :initform nil
         :accessor read-only
         :documentation "Specifies that an input field is read-only - http://www.w3schools.com/tags/att_input_readonly.asp")
   (required :initarg :required
         :initform nil
         :accessor required
         :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (size :initarg :size
         :initform nil
         :accessor size
         :documentation "Specifies the visible width, in characters, of an <input> element - http://www.w3schools.com/tags/att_input_size.asp")
   (src :initarg :src
         :initform nil
         :accessor src
         :documentation "Specifies the URL of the image to use as a submit button (only for type='image') - http://www.w3schools.com/tags/att_input_src.asp")
   (step :initarg :step
         :initform nil
         :accessor element-step
         :documentation "Specifies the legal number intervals for an input field - (* IE only from v10) - http://www.w3schools.com/tags/att_input_step.asp")
   (type :initarg :type
         :initform "text"
         :accessor element-type
         :documentation "Specifies the type <input> element to display - http://www.w3schools.com/tags/att_input_type.asp")
   (value :initarg :value
              :initform nil
              :accessor value
              :documentation "Specifies the value of an <input> element - http://www.w3schools.com/tags/att_input_value.asp")
   (width :initarg :width
              :initform nil
         :accessor width
         :documentation "Specifies the width of an <input> element (only for type='image') - http://www.w3schools.com/tags/att_input_width.asp")

   (submit-on-change :initarg :submit-on-change
                     :initform nil
                     :accessor submit-on-change
                     :documentation
                     "id of the form to submit")))

(defmethod initialize-instance :after ((input input) &key
                                                       (element-name nil element-name-p)
                                                       name
                                       &allow-other-keys)
  (declare (ignore element-name))
  (unless element-name-p
    (setf (element-name input) name)))

(defmethod render ((widget input) &key)
  (with-html
    (str
     (html-from-list
      `((:input :type ,(element-type widget)
                :name ,(name widget)
                :id ,(or (id widget) (name widget))
                ,@(get-attributes widget (list 'type 'name 'id 'value))
                :value ,(escape (value widget))))))))

(defmethod synq-data ((widget input))
  (let ((parameter (parameter (name widget))))
    (when parameter
      (setf (value widget) parameter))))

(defun on-change-function (widget)
  (if (submit-on-change widget)
      (format nil "document.getElementById(\"~a\").submit();"
              (submit-on-change widget))
      (on-change widget)))

(defclass text (input)
  ())


;;TODO: implement slots
(defclass text-area (input html-element mouse-events form-events)
  ((auto-focus :initarg :auto-focus
         :initform nil
         :accessor auto-focus
         :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (cols :initarg :cols
         :initform nil
         :accessor cols
         :documentation "Specifies the visible width of a text area - http://www.w3schools.com/tags/att_textarea_cols.asp")
   (disabled :initarg :disabled
         :initform nil
         :accessor disabled
         :documentation "Specifies that an <input> element should be disabled - http://www.w3schools.com/tags/att_input_disabled.asp")
   (form :initarg :form
         :initform nil
         :accessor form
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (max-length :initarg :max-length
         :initform nil
         :accessor max-length
         :documentation "Specifies the maximum number of characters allowed in an <input> element - http://www.w3schools.com/tags/att_input_maxlength.asp")
   (place-holder :initarg :place-holder
         :initform nil
         :accessor place-holder
         :documentation "Specifies a short hint that describes the expected value of an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_placeholder.asp")
   (read-only :initarg :read-only
         :initform nil
         :accessor read-only
         :documentation "Specifies that an input field is read-only - http://www.w3schools.com/tags/att_input_readonly.asp")
   (required :initarg :required
         :initform nil
         :accessor required
         :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (rows :initarg :rows
         :initform nil
         :accessor rows
         :documentation "Specifies the visible number of lines in a text area - http://www.w3schools.com/tags/att_textarea_rows.asp")
   (wrap :initarg :wrap
         :initform nil
         :accessor wrap
         :documentation "Specifies how the text in a text area is to be wrapped when submitted in a form - http://www.w3schools.com/tags/att_textarea_wrap.asp")))

(defmethod render ((widget text-area) &key)
  (with-html
    (str
     (html-from-list
      `((:textarea
         :name ,(name widget)
         :id ,(or (id widget) (name widget))
         ,@(get-attributes widget (list 'value 'name 'id))
         ,(escape (value widget))))))))

(defmethod synq-data ((widget text-area))
  (let ((parameter (parameter (name widget))))
    (when parameter
      (setf (value widget) parameter))))
;;;

(defclass checkbox (input)
  ((description :initarg :description
                :initform nil
                :accessor description)
   (translate-name :initarg :translate-name
                   :initform t
                   :accessor translate-name)
   (include-hidden :initarg :include-hidden
                   :initform t
                   :accessor include-hidden)))

(defmethod render ((widget checkbox) &key no-name)
  (with-slots (on-change on-click) widget
    (let ((name (if (translate-name widget)
                    (widgy-name widget "value")
                    (name widget)))
          (id (or (id widget) (name widget))))
      (with-html
        ;; When checkbox isn't checked, the browser doesn't send anything,
        ;; so we need an additional parameter which takes precedence
        ;; in this case.
        (when (and (include-hidden widget)
                   (not no-name))
          (htm
           (:input :type "hidden"
                   :name name
                   :value "off")))
        (:label :style (style widget)
                (:input :type "checkbox"
                        :name (and (not no-name)
                                   name)
                        :id (and (not no-name)
                                 id)
                        :class (css-class widget)
                        :checked (when (value widget)
                                   "checked"))
                (when (description widget)
                  (esc (description widget))))
        (when (or on-click (on-change-function widget))
          (defer-js
              (frmt "$('#~a').change(function(){~@{~@[~a;~]~}})"
                    id
                    on-click (on-change-function widget))))))))

(defmethod action-handler ((checkbox checkbox))
  (let ((value (value checkbox)))
    (when (stringp value)
      (setf (value checkbox)
            (or (string= value "on") (string= value "true") )))))

;;;

(defclass checkbox-list (input)
  ((items :initarg :items
          :initform ()
          :accessor items)
   (checkboxes :initform ()
               :accessor checkboxes)
   (orientation :initarg :orientation
                :initform :vertical
                :accessor orientation)
   (check-all :initarg :check-all
              :initform t
              :accessor check-all)
   (include-hidden :initarg :include-hidden
                   :initform t
                   :accessor include-hidden))
  (:metaclass widget-class))

(defun %create-checkboxes (checkbox-list
                           items
                           &key change
                                (parent 0)
                                (level 0))
  (loop for (nil description selected . sub) in items
        for i from 0
        for checkbox = (make-widget 'checkbox
                                    :name (sub-name checkbox-list
                                                    (frmt "~a-~a-~a"
                                                          parent level i))
                                    :description description
                                    :value selected
                                    :include-hidden (include-hidden checkbox-list))
        when change
        do (setf (value checkbox) selected
                 (description checkbox) description)
        do (setf (style checkbox)
                 (and (plusp level)
                      (frmt "padding-left:~apx" (* 10 level))))
        collect (list* checkbox
                       (%create-checkboxes checkbox-list
                                           sub
                                           :change change
                                           :parent (1+ i)
                                           :level (1+ level)))))

(defun create-checkboxes (checkbox-list
                          &key change)
  (prog1
      (setf
       (checkboxes checkbox-list)
       (%create-checkboxes checkbox-list (items checkbox-list)
                           :change change))
    (setf (value checkbox-list)
          (checkbox-list-value checkbox-list))))

(defun update-checkbox-list-items (checkbox-list)
  (labels ((%update (checkboxes items)
             (loop for (checkbox . check-sub) in checkboxes
                   for (object text nil . items-sub) in items
                   collect (list* object text (value checkbox)
                                  (%update check-sub items-sub)))))
    (setf (items checkbox-list)
          (%update (checkboxes checkbox-list)
                   (items checkbox-list)))))

(defun map-checkboxes-values (function checkbox-list)
  (labels ((recurse (items)
             (loop for (object nil value . items-sub) in items
                   do
                   (funcall function object value)
                   (recurse items-sub))))
    (recurse (items checkbox-list))))

(defun checkbox-list-value (checkbox-list)
  (labels ((%value (checkboxes items)
             (loop for (checkbox . check-sub) in checkboxes
                   for (object text value . items-sub) in items
                   when (or value check-sub)
                   collect
                   (cond ((not value)
                          (list (%value check-sub items-sub)))
                         (check-sub
                          (list* object (%value check-sub items-sub)))
                         (t
                          object)))))
    (%value (checkboxes checkbox-list)
            (items checkbox-list))))

(defmethod (setf items) :after (items (object checkbox-list))
  (create-checkboxes object :change t))

(defun all-checkboxes-checked-p (checkbox-list)
  (block nil
    (map-checkboxes-values (lambda (key value)
                             (declare (ignore key))
                             (unless value
                               (return)))
                           checkbox-list)
    t))

(defun make-check-all-button (checkbox-list)
  (when (check-all checkbox-list)
    (let ((box (make-widget 'checkbox
                            :description "Check All"
                            :css-class "checkall")))
      (setf (value box) (all-checkboxes-checked-p checkbox-list))
      (with-html
        (render box :no-name t)
        (when (eq (orientation checkbox-list) :vertical)
          (htm (:br)))))))

(defun lay-checkbox-list (checkbox-list
                          checkboxes
                          &key (level 0)
                               no-name)
  (with-html
    (loop for (checkbox . sub) in checkboxes
          do
          (render checkbox :no-name no-name)
          (lay-checkbox-list checkbox-list sub
                             :level (1+ level)
                             :no-name no-name))))

(defmethod render ((checkbox-list checkbox-list) &key no-name)
  (with-html
    (:div
     :class (ecase (orientation checkbox-list)
              (:vertical "cb-list")
              (:horizontal "horizontal-cb-list"))
     :id (name checkbox-list)
     :style (style checkbox-list)
     (make-check-all-button checkbox-list)
     (lay-checkbox-list checkbox-list
                        (create-checkboxes checkbox-list)
                        :no-name no-name)))
  (defer-js "$('#~a :input').change(function(){~@{~@[~a~]~^;~}})"
      (name checkbox-list)
    (on-change checkbox-list)
    (when (check-all checkbox-list)
      "updateCheckAll(this)")))

(defmethod action-handler ((checkbox-list checkbox-list))
  (update-checkbox-list-items checkbox-list)
  (setf (value checkbox-list)
        (checkbox-list-value checkbox-list)))

(defclass select (input)
  ((items :initarg :items
          :initform ()
          :accessor items)
   (first-item :initarg :first-item
               :initform nil
               :accessor first-item)
   (blank-allowed :initarg :blank-allowed
                  :initform t
                  :accessor blank-allowed)
   (allow-deselect :initarg :allow-deselect
                   :initform nil
                   :accessor allow-deselect)
   (auto-focus :initarg :auto-focus
               :initform nil
               :accessor auto-focus
               :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (form :initarg :form
         :initform nil
         :accessor form
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (multiple :initarg :multiple
             :initform nil
             :accessor multiple
             :documentation "Specifies that multiple options can be selected at once - http://www.w3schools.com/tags/att_select_multiple.asp")
   (required :initarg :required
             :initform nil
             :accessor required
             :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (size :initarg :size
         :initform nil
         :accessor size
         :documentation "Defines the number of visible options in a drop-down list - http://www.w3schools.com/tags/att_select_size.asp")))

(defun item-description (item)
  (if (consp item)
      (second item)
      item))

(defun item-value (item)
  (if (consp item)
      (car item)
      item))

(defun display-select-warning (select)
  (unless (or (invalid-value-p (value select))
              (not (items select)))
    (with-html
      (:img :src "/images/vfr-red-icon.png"
            :alt ""
            :title (format nil "Illegal value \"~A\"! ~
Please select a legal one"
                           (value select))))))

(defun item-equal (item value)
  (or (textually-equal-p (item-value item)
                         value)
      (textually-equal-p (item-description item)
                         value)))

(defmethod render ((widget select) &key)
  (let ((current-value (value widget))
        (first-item (first-item widget))
        selected-item)
    (with-html
      (:select
       :class (css-class widget)
       :name (element-name widget)
       :id (or (id widget) (name widget))
       :onchange (on-change-function widget)
       :deselect (allow-deselect widget)
       (when first-item
         (htm (:option :selected
                       (and (item-equal first-item current-value)
                            (setf selected-item t))
                       (esc first-item))))
       (when (and (blank-allowed widget)
                  (not (position "" (items widget) :test #'item-equal))
                  (not (position nil (items widget) :test #'item-equal)))
         (htm
          (:option
           :selected (invalid-value-p current-value))))
       (loop for item being the element of (items widget)
             do
             (htm (:option :selected
                           (and (item-equal item current-value)
                                (setf selected-item t))
                           :value (escape (item-value item))
                           (princ-esc (item-description item)))))
       (when (and (not selected-item)
                  (blank-allowed widget)
                  (not (invalid-value-p current-value)))
         (htm
          (:option
           :style "background-color:pink;"
           :selected t
           :value (escape current-value)
           (princ-esc current-value))))))))

(defclass value-select (select)
  ())

(defmethod render ((widget value-select) &key)
  (let ((current-value (value widget))
        (first-item (first-item widget))
        selected-item)
    (with-html
      (:select
       :class (css-class widget)
       :name (name widget)
       :id (or (id widget) (name widget))
       :onchange (on-change-function widget)
       (when first-item
         (htm (:option :selected
                       (and (item-equal first-item current-value)
                            (setf selected-item t))
                       (esc first-item))))
       (when (and (blank-allowed widget)
                  (invalid-value-p current-value))
         (htm
          (:option
           :selected t)))
       (loop for item being the element of (items widget)
             for i from 0
             do
             (htm (:option :selected (and (item-equal item current-value)
                                          (setf selected-item t))
                           :value i
                           (princ-esc (item-description item)))))))))

(defmethod synq-data ((widget value-select))
  (let ((index (integer-parameter (name widget))))
    (when index
      (setf (value widget)
            (item-value (nth index (items widget)))))))


(defclass labeled-input (container)
  ((label :initarg :label
          :initform nil
          :accessor label)))

(defmethod render ((widget labeled-input)
                   &key label body)
  (with-html
    (:div
     (:label (str (or label (label widget))))
     (:div
      (render (or body (content widget)))))))

(defclass meta-tag (widget html-element)
  ((meta-type :initarg :meta-type
              :initform "name"
              :accessor meta-type)
   (name :initarg :name
         :initform nil
         :accessor meta-name)
   (content :initarg :content
         :initform nil
         :accessor content)))

(defmethod render ((widget meta-tag) &key)
  (with-html
    (cond ((string-equal (meta-type widget) "charset")
           (htm (:meta :charset (content widget))))
          ((string-equal (meta-type widget) "http-equiv")
           (htm (:meta :http-equiv (name widget) :content (content widget))))
          (t
           (htm (:meta :name (meta-name widget) :content (content widget)))))))

(defclass link (widget html-element)
  ((link-type :initarg :link-type
              :initform nil
              :accessor link-type)
   (rel :initarg :rel
        :initform nil
        :accessor rel)
   (href :initarg :href
         :initform nil
         :accessor href)
   (href-lang :initarg :href-lang
         :initform nil
         :accessor href-lang)
   (media :initarg :media
         :initform nil
         :accessor media)
   (condition :initarg :condition
              :initform nil
              :accessor link-condition)))

(defmethod render ((widget link) &key)
  (with-html
    (when (link-condition widget)
      (htm (str (link-condition widget))
           (:link
            :type (link-type widget)
            :rel (rel widget)
            :href (href widget)
            :href-lang (href-lang widget)
            :media (media widget))
           "<![endif]-->"))
    (unless (link-condition widget)
      (htm (:link
            :type (link-type widget)
            :rel (rel widget)
            :href (href widget)
            :href-lang (href-lang widget)
            :media (media widget))))))

(defclass style (html-element container)
  ((style-type :initarg :style-type
              :initform "text/css"
              :accessor style-type)
   (scope :initarg :scope
        :initform nil
        :accessor scope)
   (media :initarg :media
         :initform nil
         :accessor media)))

(defmethod render ((widget style)
                   &key body)
  (with-html
    (:style
     :type (style-type widget)
     :scope (scope widget)
     :media (media widget)
     (if body
         (render body)
         (render (content widget))))))

(defclass script (html-element container)
  ((script-type :initarg :script-type
              :initform "text/javascript"
              :accessor script-type)
   (async :initarg :async
        :initform nil
        :accessor async)
   (charset :initarg :charset
         :initform nil
         :accessor charset)
   (defer :initarg :defer
         :initform nil
         :accessor defer)
   (src :initarg :src
         :initform nil
         :accessor src)
   (page-bottom-p :initarg :page-bottom-p
                  :initform nil
                  :accessor page-bottom-p)
   (condition :initarg :condition
              :initform nil
              :accessor script-condition)))

(defmethod render ((widget script)
                   &key body)
  (with-html
    (when (script-condition widget)
      (htm
       (str (script-condition widget))
       (:script
        :type (script-type widget)
        :async (async widget)
        :charset (charset widget)
        :defer (defer widget)
        :src (src widget)
        (if body
            (render body)
            (if (content widget)
                (render (content widget))))
        "<![endif]-->")))
    (unless (script-condition widget)
      (htm
       (:script
        :type (script-type widget)
        :async (async widget)
        :charset (charset widget)
        :defer (defer widget)
        :src (src widget)
        (if body
            (render body)
            (if (content widget)
                (render (content widget)))))))
    ))

(defclass bare-page (container)
  ((doc-type :initarg :doc-type
             :initform "html"
             :accessor doc-type)
   (manifest :initarg :manifest
             :initform nil
             :accessor manifest
             :documentation "Specifies the address of the document's cache manifest (for offline browsing) - http://www.w3schools.com/tags/att_html_manifest.asp")
   (base :initarg :base
         :initform nil
         :accessor base)
   (title :initarg :title
          :initform (error "An html page must have a title.")
          :accessor title)
   (author :initarg :author
           :initform ""
           :accessor author)
   (description :initarg :description
                :initform ""
                :accessor description)
   (classification :initarg :classification
                   :initform ""
                   :accessor classification)
   (key-words :initarg :key-words
              :initform ""
              :accessor key-words)
   (meta-tags :initarg :meta-tags
              :initform nil
              :accessor meta-tags)
   (links :initarg :links
          :initform nil
          :accessor links)
   (styles :initarg :styles
           :initform nil
           :accessor styles)
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts)
   (no-script :initarg :no-script
              :initform nil
              :accessor no-script)
   (body-class :initarg :body-class
               :initform nil
               :accessor body-class)))

(defclass page (bare-page)
  ())


(defmethod render ((widget bare-page) &key body)
  (with-html-string
    (fmt "<!doctype ~A>" (doc-type widget))
    (:html
     :manifest (manifest widget)
     (:head
      ;;TODO: implement as html-element (global)
      (when (base widget)
        (htm (:base :href (base widget))))
      ;;TODO: implement as html-element (global)
      (:title (render (title widget)))
      ""
      (dolist (meta (meta-tags widget))
        (render meta))
      ""
      (dolist (link (links widget))
        (render link))
      ""
      (page-include-css)
      ""
      (dolist (style (styles widget))
        (render style))
      ""
      (dolist (script (scripts widget))
        (unless (page-bottom-p script)
          (render script)))
      ""
      (when (no-script widget)
        (render (no-script widget)))
      ""
      (page-include-bits)
      ""
      (:body
       :class (body-class widget)
       (if body
           (render body)
           (render (content widget)))
       (page-include-js)

       (dolist (script (scripts widget))
         (when (page-bottom-p script)
           (render script)))
       (page-defer-include-js)
       (:script
        (when *current-page*
          (fmt "request_page(~s);"
               (request-page-id *current-page*)))
        (str (deferred-js))))))))
;;;

(defclass icon (widget)
  ((icon-name :initarg :icon-name
              :initform nil
              :accessor icon-name)
   (width :initarg :width
          :initform nil
          :accessor width)
   (height :initarg :height
           :initform nil
           :accessor height)
   (alt :initarg :alt
        :initform nil
        :accessor alt)
   (title :initarg :title
          :initform nil
          :accessor title)))

(defun make-icon (name &key (size 16)
                            (title ""))
  (render (make-widget 'icon :name (frmt "~@{~a-~}" name size title)
                             :icon-name name
                             :width size
                             :height size
                             :alt title
                             :title title)))
