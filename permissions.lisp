(in-package :wfx)

(defvar *current-permissions*)

(defstruct (permission
            (:constructor make-permission (page &optional sub-permissions)))
  page
  sub-permissions
  entities)

(defstruct (current-permissions
            (:constructor make-current-permissions (permissions)))
  (permissions nil :type hash-table)
  (time (get-universal-time) :type (unsigned-byte 64)))

(defgeneric render-permission-denied-page (acceptor &key))

(define-condition permission-denied ()
  ())

(defun call-with-permissions (function)
  (handler-case (funcall function)
    (permission-denied ()
      (if (get-parameter "token")
          (abort-api-request "Permission denied")
          (render-permission-denied-page (acceptor*))))))

(defmacro with-permissions (&body body)
  `(call-with-permissions
    (lambda () ,@body)))

(defun uri-parameters (uri)
  (let ((? (position #\? uri)))
    (if ?
        (values (subseq uri 0 ?)
                (parse-query-string (subseq uri (1+ ?))))
        (values uri nil))))

(defun check-parameter-permission (uri user-permission parameters)
  (let ((page-permissions (find uri (permissions *acceptor*) :key #'car
                                                             :test #'equal)))
    (loop for sub-permission in (cdr page-permissions)
          never (and (consp sub-permission)
                     (not (assoc (format nil "&~a = :ANY" (car sub-permission)) user-permission
                                 :test #'equal))
                     (let ((parameter (assoc-value (car sub-permission) parameters)))
                       (if parameter
                           (not (assoc (format nil "&~a = ~a" (car sub-permission) parameter)
                                       user-permission :test #'equal))
                           (not (assoc (format nil "&~a = :SOME" (car sub-permission)) user-permission
                                       :test #'equal))))))))

(defun check-permission (uri &optional sub-permission
                                       get-parameters)
  (multiple-value-bind (uri parameters)
      (typecase get-parameters
        ((eql t) (values uri (get-parameters*)))
        (cons
         (values uri get-parameters))
        (t
         (uri-parameters uri)))
    (let ((user (current-user)))
      (cond ((not user)
             nil)
            ((super-user-p user)
             t)
            (t
             (let ((permission (gethash uri (current-permissions-permissions
                                             *current-permissions*))))
               (and permission
                    (check-parameter-permission uri (permission-sub-permissions permission)
                                                parameters)
                    (or (not sub-permission)
                        (assoc sub-permission (permission-sub-permissions permission)
                               :test #'equal)))))))))

(defun check-permission-or-error (uri &optional sub-permission get-parameters)
  (or (check-permission uri sub-permission get-parameters)
      (signal 'permission-denied)))

(defun check-page-permission (&optional sub-permission (get-parameters t))
  (check-permission (script-name*) sub-permission get-parameters))

(defun check-page-permission-or-error (&optional sub-permission)
  (check-permission-or-error (script-name*) sub-permission t))

(defun add-permission (acceptor name permissions)
  (check-type name string)
  (setf (alexandria:assoc-value (permissions acceptor) name :test #'equal)
        permissions))
