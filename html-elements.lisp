(in-package :wfx)

(defclass html-element ()
  ((access-key :initarg :access-key
         :initform nil
         :accessor access-key
         :documentation "Specifies a shortcut key to activate/focus an element -http://www.w3schools.com/tags/att_global_accesskey.asp")
   (class :initarg :class
              :initform nil
              :accessor css-class
              :documentation "Specifies one or more classnames for an element (refers to a class in a style sheet) - http://www.w3schools.com/tags/att_global_class.asp")
   (content-editable :initarg :content-editable
         :initform nil
         :accessor content-editable
         :documentation "Specifies whether the content of an element is editable or not - http://www.w3schools.com/tags/att_global_contenteditable.aspp")
   (context-menu :initarg :context-menu
         :initform nil
         :accessor context-menu
         :documentation "Specifies a context menu for an element. The context menu appears when a user right-clicks on the element (* No browser support) - http://www.w3schools.com/tags/att_global_contextmenu.asp")
   (dir :initarg :dir
         :initform nil
         :accessor dir
         :documentation "Specifies the text direction for the content in an element - http://www.w3schools.com/tags/att_global_dir.asp")
   (draggable :initarg :draggable
         :initform nil
         :accessor draggable
         :documentation "Specifies whether an element is draggable or not - http://www.w3schools.com/tags/att_global_draggable.asp")
   (dropped-zone :initarg :dropped-zone
         :initform nil
         :accessor dropped-zone
         :documentation "Specifies whether the dragged data is copied, moved, or linked, when dropped (* No browser support) - http://www.w3schools.com/tags/att_global_dropzone.asp")
   (hidden :initarg :hidden
         :initform nil
         :accessor hidden
         :documentation "Specifies that an element is not yet, or is no longer, relevant (* No support in IE) - http://www.w3schools.com/tags/att_global_hidden.asp")
   (id :initarg :id
       :initform nil
       :accessor id
       :documentation "Specifies a unique id for an element - http://www.w3schools.com/tags/att_global_id.asp")
   (lang :initarg :lang
         :initform nil
         :accessor lang
         :documentation "Specifies the language of the element's content - http://www.w3schools.com/tags/att_global_lang.asp - http://www.w3schools.com/tags/ref_language_codes.asp")
   (spell-check :initarg :spell-check
         :initform nil
         :accessor spell-check
         :documentation "Specifies whether the element is to have its spelling and grammar checked or not (* IE only from v10) - http://www.w3schools.com/tags/att_global_spellcheck.asp")
   (style :initarg :style
          :initform nil
          :accessor style
          :documentation "http://www.w3schools.com/tags/att_global_spellcheck.asp - http://www.w3schools.com/tags/att_global_style.asp")
   (tab-index :initarg :tab-index
         :initform nil
         :accessor tab-index
         :documentation "Specifies the tabbing order of an element - http://www.w3schools.com/tags/att_global_tabindex.asp")
   (title :initarg :title
         :initform nil
         :accessor title
         :documentation "Specifies extra information about an element - http://www.w3schools.com/tags/att_global_title.asp")
   (translate :initarg :translate
         :initform nil
         :accessor translate
         :documentation "Specifies whether an element's value are to be translated when the page is localized, or not. - http://rishida.net/blog/?p=831")))