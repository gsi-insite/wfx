(in-package :wfx)

(defvar *edit-button*)
(defvar *delete-button*)
(defvar *new-bottom-button*)

(defvar *script-error*)

(defclass grid (ajax-widget)
  ((title :initarg :title
          :initform nil
          :accessor title)
   (icon :initarg :icon
         :initform nil
         :accessor icon)
   (columns
    :initarg :columns :initform nil
    :accessor columns)
   (row-object-class
    :initarg :row-object-class :initform nil
    :accessor row-object-class)
   (rows
    :initarg :rows :initform nil
    :accessor rows)
   (total-row-count :initarg :total-row-count
                    :initform nil
                    :accessor total-row-count)
   (rows-per-page
    :initarg :rows-per-page
    :initform 10
    :accessor rows-per-page)
   (editable
    :initarg :editable
    :initform t
    :accessor editable)
   (table-type
    :initarg :table-type
    :initform "table"
    :accessor table-type)
   (editing-row
    :initform nil
    :accessor editing-row
    :accessor selected-row)
   (edit-form
    :initform nil
    :accessor edit-form)
   (error-message
    :initform nil
    :accessor error-message)
   (delete-message
    :initarg :delete-message
    :initform "Delete document?"
    :accessor delete-message)
   (allowed-actions
    :initarg :allowed-actions
    :initform nil
    :accessor allowed-actions)
   (editor :initarg :editor
           :initform nil
           :accessor editor)
   (search-term :initarg :search-term
                :initform nil
                :accessor search-term)
   (search-script :initarg :search-script
                  :initform nil
                  :accessor search-script)
   (script-key :initarg :script-key
               :initform nil
               :accessor script-key)
   (sort-direction :initarg :sort-direction
                   :initform :ascending
                   :accessor sort-direction)
   (sort-column :initarg :sort-column
                :initform nil
                :accessor sort-column)
   (sort-key-function :initarg :sort-key-function
                      :initform nil
                      :accessor sort-key-function)
   (not-sorting-columns :initarg :not-sorting-columns
                        :initform nil
                        :accessor not-sorting-columns)
   (initial-sort-column :initarg :initial-sort-column
                        :initform '(0 :ascending)
                        :accessor initial-sort-column
                        :documentation "(column direction)")
   (grid-filter :initarg :grid-filter
                :initform nil
                :accessor grid-filter)
   (state :initarg :state
          :initform nil
          :accessor state)
   (parent-grid :initarg :parent-grid
                :initform nil
                :accessor parent-grid)
   (css-span :initarg :css-span
             :initform nil
             :accessor css-span)
   (action-widget :initarg :action-widget
                  :initform nil
                  :accessor action-widget)
   (toolbar-widget :initarg :toolbar-widget
                   :initform nil
                   :accessor toolbar-widget)
   (prefix-buttons :initarg :prefix-buttons
                   :initform nil
                   :accessor prefix-buttons) 
   (buttons :initarg :buttons
            :initform (list *edit-button* *delete-button*)
            :accessor buttons)
   (additional-buttons :initarg :additional-buttons
                       :initform nil
                       :accessor additional-buttons)
   (bottom-buttons :initarg :bottom-buttons
                   :initform (list *new-bottom-button*)
                   :accessor bottom-buttons)
   (action :initarg :action
           :initform nil
           :accessor action)
   (box :initarg :box
        :initform t
        :accessor box)
   (js-callback :initarg :js-callback
                :initform nil
                :accessor js-callback)
   (sortable :initarg :sortable
             :initform t
             :accessor sortable)
   (searchable :initarg :searchable
               :initform t
               :accessor searchable)
   (modal-edit :initarg :modal-edit
               :initform nil
               :accessor modal-edit)
   (filter-widget :initarg :filter-widget
                  :initform nil
                  :accessor filter-widget)
   (filter-first-item :initarg :filter-first-item
                      :initform t
                      :accessor filter-first-item)
   (read-only :initarg :read-only
              :initform nil
              :accessor read-only)
   (top-level :initarg :top-level
              :initform nil
              :accessor top-level)
   (advanced-search :initarg :advanced-search
                    :initform t
                    :accessor advanced-search)
   (advanced-search-slot-package :initarg :advanced-search-slot-package
                                 :initform t
                                 :accessor advanced-search-slot-package)
   (fixed-header :initarg :fixed-header
                 :initform nil
                 :accessor fixed-header))
  (:metaclass widget-class))

(defclass grid-column ()
  ((header :initarg :header
           :initform nil
           :accessor header)
   (name :initarg :name
         :initform nil
         :accessor name)
   (printer :initarg :printer
            :initform nil
            :accessor printer)
   (special-printer :initarg :special-printer
                    :initform nil
                    :accessor special-printer)
   (style :initarg :style
          :initform nil
          :accessor style)
   (width :initarg :width
          :initform nil
          :accessor width)
   (sort-key :initarg :sort-key
             :initform nil
             :accessor sort-key)
   (should-escape :initarg :escape
                  :initform t
                  :accessor should-escape)))

(defmethod initialize-instance :after ((column grid-column) &key)
  (with-slots (header name) column
    (when (and name (not header))
      (setf header (make-name name)))))

(defclass grid-button ()
  ((action :initarg :action
           :initform nil
           :accessor action)
   (url :initarg :url
        :initform nil
        :accessor url)
   (text :initarg :text
         :initform nil
         :accessor text)
   (icon :initarg :icon
         :initform nil
         :accessor icon)
   (icon-size :initarg :icon-size
              :initform 16
              :accessor icon-size)
   (hint :initarg :hint
         :initform nil
         :accessor hint)
   (confirm :initarg :confirm
            :initform nil
            :accessor confirm)
   (test :initarg :test
         :initform nil
         :accessor test)
   (permission :initarg :permission
               :initform nil
               :accessor permission)
   (before-submit :initarg :before-submit
                  :initform nil
                  :accessor before-submit)
   (writep :initarg :writep
            :initform nil
            :accessor writep)))

(defclass grid-bottom-button (grid-button)
  ((parameters :initarg :parameters
               :initform nil
               :accessor parameters)))

(defclass grid-filter ()
  ((name :initarg :name
         :initform nil
         :accessor name))
  (:metaclass xdb2:storable-class))

(defmethod print-object ((button grid-button) stream)
  (print-unreadable-object (button stream :type t :identity t)
    (princ (or (action button)
               (url button)
               "")
           stream)))

(defmethod render ((button grid-button)
                   &key row-id grid row)
  (with-slots (hint icon text action url confirm
               test before-submit) button
    (when (or (null test)
              (funcall test grid row))
      (with-html-no-indent
        (:a :class "grid-button"
            :href
            (cond (action
                   (js-link
                    (frmt "grid_action(~s,~s,~s,~s,~s~@[,~a~]~:[~;,true~])"
                          (script-name*)
                          (name (editor grid))
                          (name grid)
                          action
                          row-id
                          (or (and confirm
                                   (prin1-to-string (delete-message grid)))
                              (and before-submit
                                   "null"))
                          before-submit)))
                  ((functionp url)
                   (funcall url grid row row-id))
                  (t url))
            :target (and url "_blank")
            (when icon
              (make-icon icon :title hint
                              :size (icon-size button)))
            (when text
              (str text)))))))

(defmethod render ((button grid-bottom-button)
                   &key grid)
  (with-slots (hint icon text action url confirm
               test parameters) button
    (when (or (null test)
              (funcall test grid))
      (with-html
        (:a :href
            (cond (action
                   (let ((code
                           (apply #'js-render (editor grid)
                                  (js-pair "grid-name" (name grid))
                                  (js-pair "action" action)
                                  (if (functionp parameters)
                                      (funcall parameters)
                                      parameters))))
                     (js-link
                      (if confirm
                          (format nil
                                  "if(confirm(\"~A\")){~a}"
                                  (delete-message grid)
                                  code)
                          code))))
                  ((functionp url)
                   (funcall url grid))
                  (t url))
            :target (and url "_blank")
            (make-icon icon
                       :title hint
                       :size (icon-size button))
            (when text
              (esc text)))))))

(defparameter *edit-button*
  (make-instance 'grid-button
                 :action "edit"
                 :icon "card--pencil"
                 :hint "View/Edit"))

(defparameter *view-button*
  (make-instance 'grid-button
                 :action "edit"
                 :icon "card"
                 :hint "View"))

(defparameter *delete-button*
  (make-instance 'grid-button
                 :action "delete"
                 :icon "card--minus"
                 :hint "Delete"
                 :confirm t
                 :permission "Delete"
                 :writep t))

(defparameter *new-bottom-button*
  (make-instance 'grid-bottom-button
                 :action "new"
                 :icon "plus"
                 :icon-size 32
                 :hint "New"
                 :writep t))

;;;

(define-condition grid-redirect (condition)
  ((target :initarg :target
           :initform nil
           :accessor target)))

(define-condition grid-alert (condition)
  ((text :initarg :text
         :initform nil
         :accessor text)))

(defun grid-redirect (target)
  (signal 'grid-redirect :target target))

(defun grid-alert (text)
  (signal 'grid-alert :text text))

(define-condition grid-error (simple-error)
  ())

(defun grid-error (control-string &rest args)
  (error 'grid-error :format-control control-string
                     :format-arguments args))

(defgeneric delete-row (grid row))
(defgeneric row-count (grid))
(defgeneric get-rows (grid))

(defmethod get-rows :after ((grid grid))
  (setf (total-row-count grid) (length (rows grid))))

(defmethod row-count ((grid grid))
  (length (rows grid)))

(defun set-current-row (grid)
  (let ((row-id (integer-parameter "row_id")))
    (when row-id
      (setf (editing-row grid)
            (elt (rows grid) row-id)))))

(defmethod handle-action :before ((grid grid) action)
  (when (logging-p (acceptor*))
    (format *debug-io* "~a ~a~%" action grid))
  (when (next-method-p)
    (setf (error-message grid) nil)
    (set-current-row grid)
    (when (edit-form grid)
      (setf (error-message (edit-form grid)) nil))
    (call-next-method)))

(defmethod handle-action ((grid grid) (action (eql :delete)))
  (error "not implemented"))

(defmethod handle-action :after ((grid grid) (action (eql :delete)))
  (finish-editing grid)
  (update-table grid))

(defmethod handle-action ((grid grid) (action (eql :new)))
  (when (editable grid)
    (let ((class (or (row-object-class grid)
                     (and (plusp (length (rows grid)))
                          (class-of (elt (rows grid) 0))))))
      (setf (editing-row grid)
            (multiple-value-call #'make-instance
              class
              (if (and (top-level grid)
                       (subtypep class 'xdb2:storable-object))
                  (values :top-level t)
                  (values)))))))

(defmethod handle-action ((grid grid) (action (eql :edit))))

(defmethod handle-action ((grid grid) (action (eql :cancel)))
  (setf (edit-form grid) nil)
  (finish-editing grid))

(defmethod action-handler ((grid grid))
  (let ((action (find-symbol (string-upcase (parameter "action"))
                             #.(find-package :keyword))))
    (cond ((not (equal (name grid)
                       (parameter "grid-name"))))
          ((and (grid-read-only grid)
                (member action '(:save :delete)))
           (setf (error-message grid) "Read only grid."))
          (t
           (setf (error-message grid) nil)
           (set-current-row grid)
           (when (edit-form grid)
             (setf (error-message (edit-form grid)) nil))
           (setf (action grid) action)
           (handler-case (handle-action grid action)
             (grid-error (c)
               (setf (error-message grid)
                     (princ-to-string c)))
             (grid-redirect (c)
               (defer-js "window.location.href = '~a'" (target c))))))))

(defun render-grid-header (widget)
  (with-html
    (:thead
     (:tr
      (when (prefix-buttons widget)
        (htm (:th)))
      (dolist (column (columns widget))
        (htm (:th :style (if (width column)
                             (format nil "width:~A;" (width column)))
              (str (header column)))))
      (when (editable widget)
        (htm (:th)))))))


(defun test-allowed-action (allowed-actions action)
  (if allowed-actions
       (find action allowed-actions)
       t))

(defun test-grid-action (grid row-id action)
  (let* ((row-id (ensure-parse-integer row-id))
         (doc (and row-id
                   (elt (rows grid) row-id)))
         (action-symbol (find-symbol (string-upcase action)
                                     #.(find-package :keyword))))
    (cond ((not doc)
           (frmt "No ~a doc" row-id))
          ((not action-symbol)
           (frmt "No ~s action" action))
          (t
           (test-action grid doc action-symbol)))))

(defgeneric test-action (grid doc action))

(defun render-grid-rows (grid)
  (with-html
    (:table :width "100%"
        :id (sub-name grid "table")
      :class "table"
      (render-grid-header grid)
      (:tbody
       (:td :colspan (- (length (columns grid))
                         (if (editable grid)
                             0
                             1))
             "Loading data")))))

(defun scroll-to (widget)
  (unless (post-parameter "no-scroll")
    (frmt "scroll_to('~a')"
          (if (typep widget 'widget)
              (name widget)
              widget))))

(defclass grid-editor (ajax-widget)
  ((grid :initarg :grid
         :initform nil
         :accessor grid)
   (modal :initarg :modal
          :initform nil
          :accessor modal))
  (:metaclass widget-class))

(defun top-grid (grid)
  (let ((result grid))
    (loop for parent = (parent-grid result)
          while parent
          do
          (setf result parent))
    result))

(defun grid-read-only (grid)
  (read-only (top-grid grid)))

(defun ancestor-grids (grid)
  (loop for parent = (parent-grid grid) then (parent-grid parent)
        while parent
        collect parent))

(defun update-one-table (grid)
  (defer-js (frmt "updateTable('~a-table')" (name grid))))

(defun update-table (grid)
  (mapcar #'update-one-table (ancestor-grids grid))
  (update-one-table grid))

(defmethod open-dialog (widget grid &key (width 900) (height 500))
  (defer-js (format nil "$('#~a').dialog({width: ~A, height: ~A,~@
                                           close: function(){~a}})"
                    (name widget)
                    width height
                    (js-render (editor grid)
                               (js-pair "grid-name" (name grid))
                               (js-pair "action" "cancel")))))

(defmethod close-dialog (editor grid)
  (defer-js (format nil "$('#~a').dialog('close')"
                    (name editor))))

(defgeneric render-row-editor (grid row))

(defun render-bottom-buttons (grid)
  (when (and (bottom-buttons grid)
             (editable grid))
    (loop with read-only = (grid-read-only grid)
          for button in (bottom-buttons grid)
          unless (and read-only
                      (writep button))
          do
          (render button :grid grid))))

(defmethod render ((editor grid-editor) &key)
  (let* ((grid (grid editor))
         (editing-row (editing-row grid))
         (modal-p (modal editor)))
    (cond (editing-row
           (with-html
             (when (error-message grid)
               (htm
                (:div :class "edit-form-error"
                      (esc (error-message grid)))))
             (if (action-widget grid)
                 (render (action-widget grid))
                 (render-row-editor grid editing-row))
             (cond (modal-p
                    (open-dialog editor grid))
                   (t
                    (defer-js (scroll-to editor)))))
           (setf (state grid) :editing))
          ((eql (state grid) :editing)
           (unless (equal (parameter "action") "cancel")
             (update-table grid))
           (if modal-p
               (close-dialog editor grid)
               (defer-js (scroll-to editor)))
           (setf (state grid) nil)
           (render-bottom-buttons grid))
          (t
           (render-bottom-buttons grid)))))

(defgeneric init-data-table (grid))
(defgeneric render-header-buttons (grid))
(defgeneric render-inline-buttons (grid))
(defgeneric render-export-button (grid))

(defmethod render-inline-buttons (grid))

(defmethod render-header-buttons (grid)
  (render (make-widget 'grid-filter-selector
                       :name (sub-name grid "filter-selector")
                       :grid grid)))

(defmethod render-export-button (grid))

(defmethod render ((grid grid) &key)
  (let ((editor (make-widget 'grid-editor
                             :name (sub-name grid "editor")
                             :grid grid
                             :modal (modal-edit grid))))
    (setf (editor grid) editor)
    (with-html
      (if (box grid)
          (let ((box (make-widget 'box
                                  :icon (icon grid)
                                  :name (sub-name grid "box")
                                  :collapsible t)))
            (setf (header-content box)
                  (with-html-string
                    (render-header-buttons grid))
                  (header box) (title grid))
            (setf (content box)
                  (with-html-string (render-grid-rows grid)))
            (render box))
          (render-grid-rows grid))
      (render editor)
      (init-data-table grid))))

(defun column-value (doc column)
  (let ((name (name column)))
    (etypecase name
      (symbol
       (and (slot-exists-p doc name)
            (slot-value doc name)))
      (function
       (funcall name doc)))))

(defun column-text (grid row column &key row-id)
  (let ((value (column-value row column)))
    (princ-to-string
     (cond ((special-printer column)
            (funcall (special-printer column) grid row value row-id))
           ((not value) "")
           ((printer column)
            (funcall (printer column) value))
           (t
            value)))))

(defun expand-columns (grid row row-id)
  (loop for column in (columns grid)
        for text = (column-text grid row column :row-id row-id)
        collect (if (should-escape column)
                    (escape text)
                    text)))

(defun expand-all-columns (grid rows current-index)
  (loop for row being the elements of rows
        for row-id from current-index
        collect (expand-columns grid row row-id)))

(defun grid-filtered-rows (grid)
  (setf (rows grid)
        (sort-data grid
                   (multiple-value-bind (data filtered)
                       (apply-grid-filter grid (grid-filter grid))
                     (cond (filtered
                            (setf (total-row-count grid) (length data))
                            data)
                           (t
                            (get-rows grid)))))))

(defgeneric render-row-actions (grid row row-id))

(defmethod render-row-actions (grid row row-id)
  (let ((first t)
        (read-only (grid-read-only grid)))
   (flet ((render-button (button)
            (with-slots (permission) button
              (when (and (or (not read-only)
                             (not (writep button)))
                         (or (not permission)
                             (check-page-permission permission)))
                (unless (shiftf first nil)
                  (princ " "))
                (render button :grid grid :row-id row-id
                               :row row)))))
     (mapc #'render-button (buttons grid))
     (mapc #'render-button (additional-buttons grid)))))

(defmethod render-prefix-row-actions (grid row row-id)
  (let ((first t)
        (read-only (grid-read-only grid)))
    (flet ((render-button (button)
             (with-slots (permission) button
               (when (and (or (not read-only)
                              (not (writep button)))
                          (or (not permission)
                              (check-page-permission permission)))
                 (unless (shiftf first nil)
                   (princ " "))
                 (render button :grid grid :row-id row-id
                                :row row)))))
      (mapc #'render-button (prefix-buttons grid)))))

(defmethod get-data-table-rows ((grid grid) &key (start 0)
                                                 length)
  (let* ((data (grid-filtered-rows grid))
         (data-length (length data))
         (start (max 0 (min start (1- data-length))))
         (cut (subseq data start (and length
                                      (min data-length
                                           (+ start length))))))
    (values
     (loop for row being the elements of cut
           for expanded-row in (expand-all-columns grid cut start)
           for id from start
           collect
           (append (and (prefix-buttons grid)
                        (list (with-html-string (render-prefix-row-actions grid row id))))
                   expanded-row
                   (list (with-html-string (render-row-actions grid row id)))))
     data-length)))

(defun comparison> (x y)
  (typecase x
    (string
     (and (stringp y)
          (string> x y)))
    (number
     (and (numberp y)
          (> x y)))
    (t
     (string> (princ-to-string x)
              (princ-to-string y)))))

(defun comparison< (x y)
  (typecase x
    (string
     (and (stringp y)
          (string< x y)))
    (number
     (and (numberp y)
          (< x y)))
    (t
     (string< (princ-to-string x)
              (princ-to-string y)))))

(defun sort-direction-function (grid)
  (if (eq (sort-direction grid) :ascending)
      #'comparison<
      #'comparison>))

(defun search-columns (search-term grid row)
  (loop for column in (columns grid)
        thereis (search search-term (column-text grid row column)
                        :test #'equalp)))

(defun apply-search-script (grid data key)
  (let* ((script (search-script grid))
         (var-cons (cons "x" nil))
         (vars (list var-cons)))
    (declare (dynamic-extent vars var-cons))
    (handler-case
        (if (script-key grid)
            (loop with script-key = (alexandria:ensure-function (script-key grid))
                  for doc being the element of data
                  do (setf (cdr var-cons) (funcall script-key doc))
                  when (funcall script vars)
                  collect (cons doc (funcall key doc)))
            (loop for doc being the element of data
                  do (setf (cdr var-cons) doc)
                  when (funcall script vars)
                  collect (cons doc (funcall key doc)))) 
      (error (c)
        (setf *script-error*
              (list "script-error"
                    (replace-newlines (with-html-string-no-indent
                                        (:div :class "alert alert-danger"
                                              (:span :class "glyphicon glyphicon-exclamation-sign")
                                              "Error executing:"
                                              (:p (esc (princ-to-string c))))))))
        nil))))

(defun prepare-data (grid data)
  (let* ((column-number (sort-column grid))
         (column (and column-number
                      (elt (columns grid) column-number)))
         (key (if column
                  (or (and (eq (sort-key column) :verbatim)
                           (lambda (row)
                             (column-value row column)))
                      (sort-key column)
                      (and (zerop column-number)
                           (sort-key-function grid))
                      (lambda (row) (column-text grid row column)))
                  #'identity)))
    (cond ((search-script grid)
           (apply-search-script grid data key))
          ((search-term grid)
           (loop with search-term = (search-term grid)
                 for doc being the element of data
                 when (search-columns search-term grid doc)
                 collect (cons doc (funcall key doc))))
          (t
           (loop for doc being the element of data
                 collect (cons doc (funcall key doc)))))))

(defun sort-data (grid rows)
  (let ((sorted (sort (prepare-data grid rows)
                      (sort-direction-function grid)
                      :key #'cdr)))
    (map-into sorted #'car sorted)))

(defun compile-grid-script (script grid &optional (error-id "script-error"))
  (let* ((class (row-object-class grid))
         (slots (and class
                     (class-slots class))))
    (when slots
      (handler-case
          (compile nil `(lambda (variables)
                          (declare (ignorable variables))
                          ,(compile-script script '("x")
                                           :slots (mapcar #'c2mop:slot-definition-name slots)
                                           :slot-package (advanced-search-slot-package grid))))
        (error (c)
          (setf *script-error*
                (list error-id
                      (replace-newlines (with-html-string-no-indent
                                          (:div :class "alert alert-danger"
                                                (:span :class "glyphicon glyphicon-exclamation-sign")
                                                "Error compiling:"
                                                (:p (esc (princ-to-string c))))))))
          nil)))))

(defmethod process-data-table ((grid grid))
  (let (*script-error*)
    (handler-case
        (with-parameters ((draw "draw")
                          (start "start")
                          (length "length")
                          (search "search[value]")
                          (sort-column "order[0][column]")
                          (sort-direction "order[0][dir]")
                          script)
          (setf (search-term grid)
                (unless (equal search "")
                  search))
          (setup-filter grid)
          (setf (search-script grid)
                (and script
                     (compile-grid-script script grid)))
        
          (when (and (setf (sort-column grid) (ensure-parse-integer sort-column))
                     (prefix-buttons grid))
            (decf (sort-column grid)))
          (setf (sort-direction grid)
                (if (equal sort-direction "asc")
                    :ascending
                    :descending))
          (multiple-value-bind (data data-length)
              (get-data-table-rows grid
                                   :start (or (ensure-parse-integer start)
                                              0)
                                   :length (or (ensure-parse-integer length)
                                               10))
            (json:encode-json-plist-to-string
             (list* "draw" draw
                    "recordsTotal" (total-row-count grid)
                    "recordsFiltered" data-length
                    "data" (or data #())
                    *script-error*))))
      (grid-redirect (c)
        (frmt "[\"\", \"window.location.replace('~a');\"]"
              (target c)))
      (grid-alert (c)
        (frmt "[\"\", \"alert('~a')\"]"
              (text c))))))

(defclass grid-edit-form (widget)
  ((object :initarg :object
           :initform nil
           :accessor object)
   (validation-list :initarg :validation-list
                    :initform nil
                    :accessor validation-list)
   (error-message :initform nil
                  :accessor error-message)
   (save-disabled-p :initarg :save-disabled-p
                    :initform nil
                    :accessor save-disabled-p)))

(defun finish-editing (grid)
  (when (edit-form grid)
    (setf (error-message (edit-form grid)) nil))
  (setf (error-message grid) nil
        (editing-row grid) nil
        (action grid) nil
        (action-widget grid) nil))

(defmethod render ((form grid-edit-form) &key body)
  (let ((name (widgy-name form "edit-form")))
    (with-html

      (:form :class "validate"
             :action ""
             :method "post"
             :id name
             :name name
             (:div :class "content no-padding"
                   (str body))
             (:div :class "actions"
                   (:div :class "actions-left"
                         (:input :type "submit"
                                 :name "action"
                                 :disabled (save-disabled-p form)
                                 :onclick (format nil "javascript:return submitForm(\"~A\",~A)"
                                                  name
                                                  (build-validation-array
                                                   (validation-list form)))
                                 :value "Save"))
                   (:div :class "actions-right"
                         (:input :type "submit" :name "action" :value "Cancel")))
             (when (error-message form)
                     (htm
                      (:div :class "section _100 edit-form-error"
                            (str (error-message form)))))))))

(define-condition validation-error (error)
  ((error-message :initarg :message
                  :initform nil
                  :accessor error-message))
  (:report
   (lambda (c stream)
     (format stream "Validation Error~@[: ~a~]" (error-message c)))))

(defgeneric export-csv (widget))

(defmethod export-csv :around ((grid grid))
  (handler-case (call-next-method)
    (grid-redirect (c)
      (redirect (target c)))))

(defmethod export-csv ((grid grid))
  (setup-filter grid)
  (let* ((data (coerce (grid-filtered-rows grid) 'list))
         (slots (if data
                    (class-slots (class-of (elt data 0))))))
    (when data
      (with-output-to-string (stream)
        (dolist (slot slots)
          (format stream "~A|" (slot-definition-name slot)))

        (dolist (doc data)
          (format stream "~%")
          (dolist (slot slots)
            (format stream "~A|" (slot-value doc (slot-definition-name slot)))))))))

;;;

(defgeneric list-grid-filters (grid))
(defgeneric apply-grid-filter (grid filter))

(defmethod list-grid-filters (grid))

(defmethod apply-grid-filter :before (grid filter)
  ;; Reset the state
  (setf (read-only grid) nil))

(defmethod apply-grid-filter (grid filter)
  (values nil nil))

(defclass grid-filter-selector (widget)
  ((grid :initarg :grid
         :initform nil
         :accessor grid)
   (clear-filter-title :initarg :clear-filter-title
                       :initform "Select All"
                       :accessor clear-filter-title)
   (label :initarg :label
          :initform nil
          :accessor label)))

(defun setup-filter (grid)
  (let* ((param (get-parameter "filter"))
         (defined-filters (list-grid-filters grid))
         (custom-filter (and param
                             (or (find-symbol (string-upcase param)
                                              :keyword)
                                 (find param defined-filters
                                       :key (lambda (x)
                                              (if (typep x 'grid-filter)
                                                  (name x)
                                                  (string-capitalize x)))
                                       :test #'string-equal)))))
    (cond (custom-filter
           ;; a hack so that subsequent grids don't get confused
           (setf (slot-value *request* 'get-parameters)
                 (remove "filter" (get-parameters*)
                         :test #'equal
                         :key #'car))
           (values (setf (grid-filter grid) custom-filter) t))
          ((let ((select (get-widget (sub-name grid "filter-selector-select"))))
             (and select
                  (setf (grid-filter grid)
                        (find (value select) defined-filters
                              :key (lambda (x)
                                     (if (typep x 'grid-filter)
                                         (name x)
                                         x))
                              :test #'string-equal)))))
          (t
           (setf (grid-filter grid) nil)))))

(defmethod render ((widget grid-filter-selector) &key)
  (multiple-value-bind (filter custom) (setup-filter (grid widget))
    (let* ((grid (grid widget))
           (filters (if custom
                        (list filter)
                        (list-grid-filters grid))))
      (when filters
        (with-html
          (:div :class (css-class widget)
                (when (label widget)
                  (htm (:label (esc (label widget)))))
                (let ((select (make-widget 'select
                                           :name (sub-name widget "select")
                                           :blank-allowed (filter-first-item grid)
                                           :first-item (and (filter-first-item grid)
                                                            (clear-filter-title widget)))))
                  (setf (on-change select)
                        (js-render grid (js-value select)))
                  (let ((items (mapcar (lambda (x)
                                         (if (typep x 'grid-filter)
                                             (name x)
                                             (string-capitalize x)))
                                       filters)))
                    (unless (parameter (name select))
                      (setf (value select) (if custom
                                               (string-upcase filter)
                                               (if (filter-first-item grid)
                                                   (clear-filter-title widget)
                                                   (car items)))))
                   (setf (items select) items))
                  (render select))))))))
