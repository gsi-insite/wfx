(in-package :wfx)

(defvar *debug-errors* t)

(defclass site-acceptor (easy-acceptor)
  ((debug-errors-p :initarg :debug-errors-p
                   :initform nil
                   :accessor debug-errors-p)
   (logging-p :initarg :logging-p
              :initform nil
              :accessor logging-p)
   (started :initarg :started
            :initform nil
            :accessor started)
   (site-name :initarg :site-name
              :initform (error "site-name should be provided")
              :accessor site-name)
   (site-url :initarg :site-url
             :initform (error "site-url should be provided")
             :accessor site-url)
   (login-url :initarg :login-url
              :initform nil
              :accessor login-url)
   (ajax-url :initarg :ajax-url
             :initform nil
             :accessor ajax-url) 
   (data-path :initarg :data-path
              :initform nil
              :accessor data-path)
   (permissions :initarg :permissions
                :initform nil
                :accessor permissions)
   (ajax-processor :initarg :ajax-processor
                   :initform nil
                   :accessor ajax-processor)
   (theme :initarg :theme
          :initform nil
          :accessor theme)))

(defun acceptor* ()
  (hunchentoot:request-acceptor *request*))

(defmethod start :after ((acceptor site-acceptor))
  (setf (started acceptor) t))

(defmethod acceptor-log-access :around ((acceptor site-acceptor)
                                        &key &allow-other-keys)
  (when (logging-p acceptor)
    (call-next-method)))

(defun ajax-call-lisp-function (site processor)
  "This is called from hunchentoot on each ajax request. It parses the
   parameters from the http request, calls the lisp function and returns
   the response."
  (let* ((fn-name (string-trim "/" (subseq (script-name* *request*)
                                           (length (ht-simple-ajax::server-uri processor)))))
         (fn (gethash fn-name (ht-simple-ajax::lisp-fns processor)))
         (args (mapcar #'cdr (get-parameters* *request*))))
    (unless fn
      (error "Error in call-lisp-function: no such function: ~A" fn-name))

    (setf (reply-external-format*) (reply-external-format processor))
    (setf (content-type*) (content-type processor))
    (no-cache)
    (let ((*current-theme* (theme site)))
      (apply fn args))))

(defun create-ajax-dispatcher (site processor)
  "Creates a hunchentoot dispatcher for an ajax processor"
  (create-prefix-dispatcher (ht-simple-ajax::server-uri processor)
                            (lambda () (ajax-call-lisp-function site processor))))

(defun assert-ends-with-/ (string)
  (assert (char= (alexandria:last-elt string) #\/)))

(defun make-dx-site (acceptor-class
                     &key
                       port site-name site-url data-path
                       logging-p debug-errors-p login-url
                       theme ajax-url)
  (assert-ends-with-/ site-url)
  (let* ((ajax-processor
           (make-instance 'ht-simple-ajax:ajax-processor
                          :server-uri (frmt "~Aajax" site-url)))
         (site
           (make-instance
            acceptor-class
            :port port
            :site-name site-name
            :data-path data-path
            :site-url site-url
            :logging-p logging-p
            :debug-errors-p debug-errors-p
            :login-url (or login-url (frmt "~Alogin" site-url))
            :ajax-url (or ajax-url (frmt "~Aajax" site-url))
            :ajax-processor ajax-processor
            :theme theme))
         (ajax-prefix-dispatcher
           (create-ajax-dispatcher site ajax-processor)))
    (pushnew ajax-prefix-dispatcher *dispatch-table*)
    (when data-path
      (ensure-directories-exist data-path))
    site))

(defmacro define-dx-ajax (acceptor name lambda-list &body body)
  `(ht-simple-ajax:defun-ajax ,name ,lambda-list ((ajax-processor ,acceptor))
     ,@body))

;;;

(defun current-user ()
  (when (boundp '*session*)
    (session-value 'user)))

(defun (setf current-user) (user)
  (when (boundp '*session*)
    (setf (session-value 'user) user)))

(defgeneric super-user-p (user))

(defmacro define-dx-handler (acceptor description lambda-list &body body)
  (when (atom description)
    (setf description (list description)))
  (destructuring-bind (name &rest args
                       &key uri for-everyone permissions
                       &allow-other-keys) description
    `(progn
       ,(when (and uri (not for-everyone))
          `(add-permission ,acceptor ,uri (append ,permissions '("Delete All" "Advanced Search" "Delete Version"))))
       (define-easy-handler (,name ,@args :allow-other-keys t) ,lambda-list
           ,@(when (and uri (not for-everyone))
               `((check-permission-or-error ,uri nil t)))
         ,@body))))

(defgeneric render-error-page (acceptor &key))
(defgeneric login-not-required (acceptor script-name))

(defmethod render-error-page ((acceptor site-acceptor) &key condition)
  (with-html-string
    (:html (:head (:title (esc (frmt "Error - ~a" (script-name*)))))
           (:body
            (:div :class "error-description"
                  (:strong :style "color: red;"
                           "Error: ")
                  (esc (princ-to-string condition)))))))

(defmethod login-not-required ((acceptor site-acceptor) script-name)
  (equal script-name (login-url acceptor)))

(defmethod accept-connections :around ((acceptor site-acceptor))
  (handler-case (call-next-method)
    ((or stream-error usocket:socket-error
      sb-bsd-sockets:socket-error) ()
      (call-next-method))))

(defmethod hunchentoot:maybe-invoke-debugger ((condition error))
  (if (or (not (boundp '*request*))
          (debug-errors-p (acceptor*)))
      (typecase condition
        ((or stream-error usocket:socket-error))
        (t
         (invoke-debugger condition)))
      (throw 'error condition)))

(defun call-with-error-handling (function)
  (if (debug-errors-p (acceptor*))
      (funcall function)
      (let ((condition
              (catch 'error
                (funcall function))))
        (if (typep condition 'error)
            (render-error-page (acceptor*) :condition condition)
            condition))))

(defmacro with-error-handling (&body body)
  `(call-with-error-handling
    (lambda () ,@body)))

(defmethod handle-request :before ((acceptor site-acceptor) request)
  (unless (equal (script-name*) (login-url acceptor))
    (unless (equal (session-value 'current-uri) (request-uri*))
      (setf (session-value 'previous-uri) (session-value 'current-uri)
            (session-value 'previous-page) (session-value 'current-page)))
    (setf (session-value 'current-uri) (request-uri*)
          (session-value 'current-page) (script-name*))))

(defun redirect-ajax-to-login ()
  (frmt "[null,\"window.location.replace('~a')\"]"
        (login-url (acceptor*))))

(defmethod session-created :after ((acceptor site-acceptor) session)
  (setf (session-value 'pages session) (make-hash-table :test 'equal)))

(defun session-pages ()
  (session-value 'pages))

(defvar *current-page*)

(defstruct (request-page
            (:constructor make-request-page (name)))
  (name nil :type string)
  (id nil :type (or null string))
  (dom nil :type list)
  (cache (make-hash-table :test 'equal) :type hash-table)
  (new t :type boolean)
  (reuse nil :type boolean)
  permissions
  ;;get
  
  )

(defun find-page (id)
  (values (gethash id (session-pages))))

(defun generate-page-id ()
  (declare (optimize speed))
  (format nil "~:@(~36r~)" (random (expt 2 32))))

(defvar *page-id-lock* (bordeaux-threads:make-lock))

(defun generate-new-page (name)
  (let ((pages (session-pages))
        (page (make-request-page name)))
    (bordeaux-threads:with-lock-held (*page-id-lock*)
      (loop for id = (generate-page-id)
            unless (gethash id pages)
            do (setf (request-page-id page) id
                     (gethash id pages) page)
            return id))
   
   ;; (setf (request-page-get page)
     ;;     (get-parameters*))
    page))

(defgeneric setup-request-page (acceptor page))
(defmethod setup-request-page (acceptor page))

(defun ensure-request-page (request)
  (let* ((pages (session-pages))
         (id (post-parameter "pageid" request))
         (page (if id
                   (or (gethash id pages)
                       (bad-request))
                   (generate-new-page (script-name request)))))
    (setup-request-page (request-acceptor request) page)
    page))

(defun find-request-page (&optional (id (parameter "pageid")) (error t))
  (or (gethash id (session-pages))
      (and error
           (bad-request))))

(defun bad-request ()
  (setf (return-code *reply*) +http-bad-request+)
  (abort-request-handler))

(defun abort-api-request (message &optional (code +http-bad-request+))
  (setf (return-code *reply*) code)
  (abort-request-handler (json:encode-json-to-string (list (cons "error" message)))))

(defun ajax-request-p (request)
  (alexandria:starts-with-subseq (ajax-url (request-acceptor request))
                                 (script-name request)))


(defgeneric authenticate-token (theme token))

(defmethod handle-request :around ((acceptor site-acceptor) request)
  (let ((script-name (script-name request))
        (ajax (ajax-request-p request))
        (token (get-parameter "token"))
        (*current-theme* (theme acceptor)))
    (flet ((handle ()
             (let* (*print-pretty*
                    (*widget-parameters* nil)
                    (*current-permissions* (session-value 'permissions))
                    (*current-page* (and (not ajax)
                                         (ensure-request-page request))))
               (unless ajax
                 (with-debugging
                   (map-dom #'synq-dom)
                   (map-dom #'synq-data)
                   (map-dom-events)))
               (with-error-handling
                 (with-permissions
                   (call-next-method))))))
      (cond (token 
             (let* ((session (make-instance 'session))
                    (*session* session))
               (setf (session request) session
                     (session-value 'pages session)
                     (make-hash-table :test 'equal))
               (authenticate-token (theme acceptor) token)
               (handle)))
            ((or (current-user)
                 (login-not-required acceptor script-name))
             (start-session)
             (handle))
            (ajax
             (redirect-ajax-to-login))
            (t
             (setf (session-value 'redirect-after-login) (request-uri request))
             (redirect (login-url acceptor)))))))

(defmethod render-permission-denied-page ((acceptor site-acceptor) &key)
  (let ((title (frmt "Access denied - ~a" (script-name*))))
    (with-html-string
      (:html (:head (:title (esc title)))
             (:body
              (:div :class "error-description"
                    (:strong :style "color: red;"
                             "Error: ")
                    (esc title)))))))

(defun page-id ()
  (request-page-id *current-page*))

(defun page-permissions ()
  (request-page-permissions *current-page*))
